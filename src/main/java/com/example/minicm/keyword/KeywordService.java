package com.example.minicm.keyword;

import com.example.minicm.aspect.timeLogging.TimeLogged;
import com.example.minicm.keyword.helper.KeywordLookupHelper;
import com.example.minicm.keyword.helper.model.BGObject;
import com.example.minicm.keyword.helper.model.KeywordLookupRequest;
import com.example.minicm.keyword.helper.model.KeywordLookupResponse;
import com.example.minicm.keyword.helper.model.RObject;
import com.example.minicm.keyword.model.KeywordServiceRequest;
import com.example.minicm.keyword.model.KeywordServiceResponse;
import com.example.minicm.targeting.TargetingService;
import com.example.minicm.targeting.model.KeywordStyles;
import com.example.minicm.targeting.model.TargetingServiceRequest;
import com.example.minicm.utility.ip.IPUtility;
import com.example.minicm.utility.ip.model.IPUtilityDTO;
import com.example.minicm.utility.location.LocationUtility;
import com.example.minicm.utility.location.model.Location;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class KeywordService {
    private static final Logger LOG = LoggerFactory.getLogger(KeywordService.class);
    private IPUtility ipUtility;
    private LocationUtility locationUtility;
    private KeywordLookupHelper keywordLookupHelper;
    private TargetingService targetingService;

    @Autowired
    public void setDependencies(
            IPUtility ipUtility, KeywordLookupHelper keywordLookupHelper,
            LocationUtility locationUtility, TargetingService targetingService) {
        this.ipUtility = ipUtility;
        this.keywordLookupHelper = keywordLookupHelper;
        this.locationUtility = locationUtility;
        this.targetingService = targetingService;
    }

    @TimeLogged
    public KeywordServiceResponse getKeywords(KeywordServiceRequest ksReq, IPUtilityDTO ipUtilityDTO) {
        LOG.info("getKeywords()");
        LOG.debug(ksReq.toString());
        LOG.debug(ipUtilityDTO.toString());

        // Extract user's ip and location
        LOG.info("fetching user's ip");
        String userIpAddress = ipUtility.getClientIpAddress(ipUtilityDTO);
        LOG.debug("user ip: " + userIpAddress);
        LOG.info("fetching user location");
        Location userLocation = locationUtility.getLocationFromIP(userIpAddress);
        LOG.debug("user location: " + userLocation.toString());

        // Perform keyword lookup
        LOG.info("extracting keyword hint");
        String keywordHint = extractKeywordHint(ksReq.getWords());
        LOG.debug("keywordHint: " + keywordHint);
        LOG.info("looking up for keywords");
        List<BGObject> keywordList = performKeywordLookup(ksReq.getDomain(), ksReq.getUrl(), keywordHint, userLocation.getCountry());

        KeywordServiceResponse keywordServiceResponse = new KeywordServiceResponse();
        keywordServiceResponse.setKeywordList(keywordList);

        // Get rendering styles for keywords
        LOG.info("fetching keyword styles");
        keywordServiceResponse.setKeywordStylesMap(getKeywordStyles(ksReq, keywordHint, userLocation));

        return keywordServiceResponse;
    }

    private String extractKeywordHint(List<String> words) {
        Integer maxFrequency = 0;

        Map<String, Integer> keywordFrequencyMap = new HashMap<>();

        for(String word : words) {
            Integer prevVal = keywordFrequencyMap.getOrDefault(word, 0);
            keywordFrequencyMap.put(word, prevVal + 1);
            maxFrequency = (prevVal + 1 > maxFrequency)? prevVal + 1 : maxFrequency;
        }

        List<String> mostFrequentWords = new ArrayList<>();

        for(Map.Entry<String, Integer> entry : keywordFrequencyMap.entrySet()) {
            if (entry.getValue().equals(maxFrequency)) {
                mostFrequentWords.add(entry.getKey());
            }
        }

        int ri = (int) Math.round(Math.random() * (mostFrequentWords.size() - 1));

        return mostFrequentWords.get(ri);
    }

    private List<BGObject> performKeywordLookup(String domain, String url, String keywordHint, String country) {
        KeywordLookupRequest keywordLookupRequest = new KeywordLookupRequest();
        keywordLookupRequest.setDomain(domain);
        keywordLookupRequest.setUrl(url);
        keywordLookupRequest.setKeywordHint(keywordHint);
        keywordLookupRequest.setCountry(country);

        KeywordLookupResponse keywordLookupResponse = keywordLookupHelper.performKeywordLookup(keywordLookupRequest);
        final List<BGObject> kList = new ArrayList<>();

        List<List<RObject>> r = keywordLookupResponse.getR();
        if (r != null && r.size() > 0) {
            r.forEach(rList-> rList.forEach(rObject -> kList.addAll(rObject.getBg())));
        }

        return kList;
    }

    private Map<String, KeywordStyles> getKeywordStyles(KeywordServiceRequest ksReq, String keywordHint, Location userLocation) {
        Map<String, KeywordStyles> keywordStylesMap = new HashMap<>();

        ksReq.getAdTagIdList().forEach(adTagId -> {
            TargetingServiceRequest tsRequest = new TargetingServiceRequest();
            tsRequest.setCustomerId(ksReq.getCustomerId());
            tsRequest.setBrowser(ksReq.getBrowser());
            tsRequest.setOs(ksReq.getOs());
            tsRequest.setLocation(userLocation);
            tsRequest.setPattern(keywordHint);
            tsRequest.setAdTagId(adTagId);

            keywordStylesMap.put(adTagId, targetingService.getKeywordStyles(tsRequest));
        });

        return keywordStylesMap;
    }
}
