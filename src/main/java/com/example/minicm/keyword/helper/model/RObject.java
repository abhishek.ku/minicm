package com.example.minicm.keyword.helper.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class RObject {
    private int p;
    private List<BGObject> bg;

    public int getP() {
        return p;
    }

    public void setP(int p) {
        this.p = p;
    }

    public List<BGObject> getBg() {
        return bg;
    }

    public void setBg(List<BGObject> bg) {
        this.bg = bg;
    }
}
