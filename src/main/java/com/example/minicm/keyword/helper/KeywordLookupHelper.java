package com.example.minicm.keyword.helper;

import com.example.minicm.keyword.helper.model.KeywordLookupRequest;
import com.example.minicm.keyword.helper.model.KeywordLookupResponse;
import com.fasterxml.jackson.core.JsonProcessingException;

public interface KeywordLookupHelper {
    KeywordLookupResponse performKeywordLookup(KeywordLookupRequest keywordLookupRequest);
}
