package com.example.minicm.keyword.helper.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class KeywordLookupRequest {
    private String domain;
    private String url;
    private String keywordHint;
    private String country;

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getKeywordHint() {
        return keywordHint;
    }

    public void setKeywordHint(String keywordHint) {
        this.keywordHint = keywordHint;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    @Override
    public String toString() {
        return "KeywordLookupRequest{" +
                "domain='" + domain + '\'' +
                ", url='" + url + '\'' +
                ", keywordHint='" + keywordHint + '\'' +
                ", country='" + country + '\'' +
                '}';
    }
}
