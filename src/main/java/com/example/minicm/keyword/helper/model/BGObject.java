package com.example.minicm.keyword.helper.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class BGObject {
    private int c;
    private List<KObject> k;

    public int getC() {
        return c;
    }

    public void setC(int c) {
        this.c = c;
    }

    public List<KObject> getK() {
        return k;
    }

    public void setK(List<KObject> k) {
        this.k = k;
    }
}
