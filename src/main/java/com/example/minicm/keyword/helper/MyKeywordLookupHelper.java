package com.example.minicm.keyword.helper;

import com.example.minicm.aspect.timeLogging.TimeLogged;
import com.example.minicm.keyword.helper.model.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;
import org.springframework.util.ConcurrentReferenceHashMap;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.*;

@Component
public class MyKeywordLookupHelper implements KeywordLookupHelper {
    private static final Logger LOG = LoggerFactory.getLogger(MyKeywordLookupHelper.class);
    private static final Map<String, KeywordLookupResponse> responseCache = new ConcurrentReferenceHashMap<>();
    private String keywordApiBaseUrl;
    private RestTemplate restTemplate;
    private KeywordLookupResponse defaultKeywordLookupResponse;

    @Autowired
    public void setDependencies(@Qualifier("kbbRT") RestTemplate restTemplate,
                                KeywordLookupResponse keywordLookupResponse,
                                Environment env) {
        this.restTemplate = restTemplate;
        defaultKeywordLookupResponse = keywordLookupResponse;
        keywordApiBaseUrl = env.getProperty("keywordApiUrl");
    }

    @Override
    @TimeLogged
    public KeywordLookupResponse performKeywordLookup(KeywordLookupRequest klr) {
        LOG.info("performKeywordLookup()");
        LOG.debug(klr.toString());

        String url = UriComponentsBuilder.
                fromUriString(keywordApiBaseUrl).
                queryParam("d", klr.getDomain()).
                queryParam("cc", klr.getCountry()).
                queryParam("kf", "0").
                queryParam("pt", "60").
                queryParam("type", "1").
                queryParam("uftr", "0").
                queryParam("kwrd", "0").
                queryParam("py", "1").
                queryParam("dtld", "com").
                queryParam("combineExpired", "1").
                queryParam("lid", "224").
                queryParam("mcat", "4067").
                queryParam("fpid", "800200068").
                queryParam("maxno", "15").
                queryParam("actno", "10").
                queryParam("ykf", "1").
                queryParam("pid", "8POEPJG4R").
                queryParam("lmsc", "0").
                queryParam("hs", "3").
                queryParam("partnerid", "7PRFT79UO").
                queryParam("stag", "mnet_mobile435_search").
                queryParam("stags", "mnet_mobile435_search%2Cskenzo_search11%2C365657").
                queryParam("pstag", "skenzo_search11").
                queryParam("mtags", "hypecmdm%2Cperform").
                queryParam("crid", "147984832").
                queryParam("pmp", "us").
                queryParam("https", "1").
                queryParam("accrev", "1").
                queryParam("csid", "8CU2T3HV4").
                queryParam("ugd", "4").
                queryParam("rand", "1526919391").
                queryParam("tsize", "800x200").
                queryParam("calling_source", "cm").
                queryParam("fm_mo", "10").
                queryParam("fm_bt", "1").
                queryParam("bkt_block", "344").
                queryParam("stag_tq_block", "1").
                queryParam("json", "1").
                queryParam("hint", klr.getKeywordHint()).
                queryParam("rurl", klr.getUrl()).
                toUriString();

        LOG.info(url);

        KeywordLookupResponse keywordLookupResponse = null;

        if (responseCache.containsKey(url)) {
            LOG.info("cache hit");
            keywordLookupResponse = responseCache.get(url);
        } else {
            LOG.info("cache miss");
            try {
                keywordLookupResponse = restTemplate.getForObject(url, KeywordLookupResponse.class);
            } catch (RestClientException e) {
                LOG.error("error fetching response from kbb", e);
            }

            if (keywordLookupResponse != null) {
                responseCache.put(url, keywordLookupResponse);
            } else {
                keywordLookupResponse = defaultKeywordLookupResponse;
            }
        }

        return keywordLookupResponse;
    }
}
