package com.example.minicm.keyword.helper.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class KeywordLookupResponse {
    private List<List<RObject>> r;
    private int ktd;
    private int lf;
    private String rt;
    private int bid;
    private int bidBucket;
    private String kpd;

    public List<List<RObject>> getR() {
        return r;
    }

    public void setR(List<List<RObject>> r) {
        this.r = r;
    }

    public int getKtd() {
        return ktd;
    }

    public void setKtd(int ktd) {
        this.ktd = ktd;
    }

    public int getLf() {
        return lf;
    }

    public void setLf(int lf) {
        this.lf = lf;
    }

    public int getBid() {
        return bid;
    }

    public void setBid(int bid) {
        this.bid = bid;
    }

    public int getBidBucket() {
        return bidBucket;
    }

    public void setBidBucket(int bidBucket) {
        this.bidBucket = bidBucket;
    }

    public String getKpd() {
        return kpd;
    }

    public void setKpd(String kpd) {
        this.kpd = kpd;
    }

    public String getRt() {
        return rt;
    }

    public void setRt(String rt) {
        this.rt = rt;
    }
}
