package com.example.minicm.keyword.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class KeywordServiceRequest {
    private String domain;
    private String customerId;
    private List<String> adTagIdList;
    private String url;
    private String referer;
    private List<String> words;
    private String browser;
    private String os;

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public List<String> getAdTagIdList() {
        return adTagIdList;
    }

    public void setAdTagIdList(List<String> adTagIdList) {
        this.adTagIdList = adTagIdList;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getReferer() {
        return referer;
    }

    public void setReferer(String referer) {
        this.referer = referer;
    }

    public List<String> getWords() {
        return words;
    }

    public void setWords(List<String> words) {
        this.words = words;
    }

    public String getBrowser() {
        return browser;
    }

    public void setBrowser(String browser) {
        this.browser = browser;
    }

    public String getOs() {
        return os;
    }

    public void setOs(String os) {
        this.os = os;
    }

    @Override
    public String toString() {
        return "KeywordServiceRequest{" +
                "domain='" + domain + '\'' +
                ", customerId='" + customerId + '\'' +
                ", adTagIdList=" + adTagIdList +
                ", url='" + url + '\'' +
                ", referer='" + referer + '\'' +
                ", words=" + words +
                ", browser='" + browser + '\'' +
                ", os='" + os + '\'' +
                '}';
    }
}
