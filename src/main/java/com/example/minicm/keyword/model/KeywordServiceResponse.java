package com.example.minicm.keyword.model;

import com.example.minicm.keyword.helper.model.BGObject;
import com.example.minicm.targeting.model.KeywordStyles;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;
import java.util.Map;

public class KeywordServiceResponse {
    @JsonProperty("kl")
    private List<BGObject> keywordList;
    @JsonProperty("ks")
    private Map<String, KeywordStyles> keywordStylesMap;

    public List<BGObject> getKeywordList() {
        return keywordList;
    }

    public void setKeywordList(List<BGObject> keywordList) {
        this.keywordList = keywordList;
    }

    public Map<String, KeywordStyles> getKeywordStylesMap() {
        return keywordStylesMap;
    }

    public void setKeywordStylesMap(Map<String, KeywordStyles> keywordStylesMap) {
        this.keywordStylesMap = keywordStylesMap;
    }
}
