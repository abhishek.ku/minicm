package com.example.minicm.keyword;

import com.example.minicm.aspect.timeLogging.TimeLogged;
import com.example.minicm.keyword.model.KeywordServiceRequest;
import com.example.minicm.keyword.model.KeywordServiceResponse;
import com.example.minicm.utility.ip.model.IPUtilityDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

@Controller
public class KeywordController {
    private static final Logger LOG = LoggerFactory.getLogger(KeywordController.class);
    private KeywordService keywordService;

    @Autowired
    public void setDependencies(KeywordService keywordService) {
        this.keywordService = keywordService;
    }

    @RequestMapping(method = RequestMethod.POST, path = "getKeywords")
    @CrossOrigin(origins = "*")
    @TimeLogged
    public @ResponseBody KeywordServiceResponse getKeywords(
            @RequestBody KeywordServiceRequest keywordServiceRequest,
            HttpServletRequest httpServletRequest) {
        LOG.info("getKeywords()");
        Map<String, String> headerMap = new HashMap<>();

        Enumeration<String> headerNames = httpServletRequest.getHeaderNames();
        while(headerNames.hasMoreElements()) {
            String headerName = headerNames.nextElement();
            headerMap.put(headerName, httpServletRequest.getHeader(headerName));
        }

        IPUtilityDTO ipUtilityDTO = new IPUtilityDTO();
        ipUtilityDTO.setHeaderMap(headerMap);
        ipUtilityDTO.setRemoteAddress(httpServletRequest.getRemoteAddr());

        return keywordService.getKeywords(keywordServiceRequest, ipUtilityDTO);
    }
}
