package com.example.minicm.serp.helper;

import com.example.minicm.aspect.timeLogging.TimeLogged;
import com.example.minicm.serp.helper.model.Results;
import com.example.minicm.serp.helper.model.SERLookupRequest;
import com.example.minicm.serp.helper.model.SERLookupResponse;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.env.Environment;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.io.IOException;
import java.net.URI;

@Component
public class MySERLookupHelper implements SERLookupHelper {
    private static final Logger LOG = LoggerFactory.getLogger(MySERLookupHelper.class);
    private RestTemplate restTemplate;
    private String yahooApiBaseUrl;

    @Autowired
    public void setDependencies(@Qualifier("serRT") RestTemplate restTemplate, Environment env) {
        this.restTemplate = restTemplate;
        yahooApiBaseUrl = env.getProperty("yahooApiUrl");
    }

    @Override
    @TimeLogged
    public SERLookupResponse performSERLookup(SERLookupRequest lookupRequest) {
        LOG.info("performSERLookup()");
        LOG.info(lookupRequest.toString());
        LOG.debug(lookupRequest.toString());

        URI uri = UriComponentsBuilder.
                fromUriString(yahooApiBaseUrl).
                queryParam("mkt", "us").
                queryParam("Partner", "skenzo_test1").
                queryParam("maxCount", "10").
                queryParam("config", "1234567890").
                queryParam("type", "tag-ng-89715").
                queryParam("affilData", "ip%3D217.140.100.0%26ua%3DMozilla%252F5.0%2B%2528Windows%253B%2BU%253B%2BWindows%2BNT%2B5.1%253B%2Ben-US%253B%2Brv%253A1.9.2.17%2529%2BGecko%252F20110420%2BFirefox%252F3.6.17%2B%2528%2B.NET%2BCLR%2B3.5.30729%253B%2B.NET4.0E%2529%26ur%3D%26al%3Den-us%252Cen%253Bq%253D0.5").
                queryParam("bolding", "true").
                queryParam("adultFilter", "clean").
                queryParam("Keywords", lookupRequest.getClickedKeyword()).
                queryParam("serveUrl", lookupRequest.getServeURL()).
                build().toUri();

        LOG.info(uri.toString());

        RequestEntity<Void> requestEntity = RequestEntity.get(uri).header("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36").build();

        ResponseEntity<String> responseEntity = restTemplate.exchange(requestEntity, String.class);
        String responseString = responseEntity.getBody();

        ObjectMapper objectMapper = new XmlMapper();
        try {
            Results results = objectMapper.readValue(responseString, Results.class);
            SERLookupResponse response = new SERLookupResponse();
            response.setResults(results);
            return response;
        } catch (IOException e) {
            LOG.error("Error reading result xml file", e);
            return null;
        }
    }
}
