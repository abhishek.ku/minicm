package com.example.minicm.serp.helper.model;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlText;

public class ClickUrl {
    @JacksonXmlProperty(localName = "type", isAttribute = true)
    private String type;
    @JacksonXmlText()
    private String value;

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = (value != null)? value.trim() : null;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public ClickUrl(String value, String type) {
        this.value = value;
        this.type = type;
    }

    public ClickUrl() {
    }
}
