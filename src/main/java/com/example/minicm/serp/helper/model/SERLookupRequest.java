package com.example.minicm.serp.helper.model;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

public class SERLookupRequest {
    private String clickedKeyword;
    private String ip;
    private String ua;
    private String languageHeader;
    private String serveURL;

    public String getClickedKeyword() {
        return clickedKeyword;
    }

    public void setClickedKeyword(String clickedKeyword) {
        this.clickedKeyword = clickedKeyword;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getUa() {
        return ua;
    }

    public void setUa(String ua) {
        this.ua = ua;
    }

    public String getLanguageHeader() {
        return languageHeader;
    }

    public void setLanguageHeader(String languageHeader) {
        this.languageHeader = languageHeader;
    }

    public String getServeURL() {
        return serveURL;
    }

    public void setServeURL(String serveURL) {
        this.serveURL = serveURL;
    }

    @Override
    public String toString() {
        return "SERLookupRequest{" +
                "clickedKeyword='" + clickedKeyword + '\'' +
                ", ip='" + ip + '\'' +
                ", ua='" + ua + '\'' +
                ", languageHeader='" + languageHeader + '\'' +
                ", serveURL='" + serveURL + '\'' +
                '}';
    }
}
