package com.example.minicm.serp.helper.model;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

public class Listing {
    @JacksonXmlProperty(localName = "rank", isAttribute = true)
    private int rank;
    @JacksonXmlProperty(localName = "title", isAttribute = true)
    private String title;
    @JacksonXmlProperty(localName = "description", isAttribute = true)
    private String description;
    @JacksonXmlProperty(localName = "adultRating", isAttribute = true)
    private String adultRating;
    @JacksonXmlProperty(localName = "phoneNumber", isAttribute = true)
    private String phoneNumber;
    @JacksonXmlProperty(localName = "new_ecpi", isAttribute = true)
    private int newEcpi;
    @JacksonXmlProperty(localName = "siteHost", isAttribute = true)
    private String siteHost;
    @JacksonXmlProperty(localName = "ClickUrl")
    private ClickUrl clickUrl;

    @JacksonXmlProperty(localName = "ImpressionId", isAttribute = true)
    private String impressionId;

    public int getRank() {
        return rank;
    }

    public void setRank(int rank) {
        this.rank = rank;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAdultRating() {
        return adultRating;
    }

    public void setAdultRating(String adultRating) {
        this.adultRating = adultRating;
    }

    public ClickUrl getClickUrl() {
        return clickUrl;
    }

    public void setClickUrl(ClickUrl clickUrl) {
        this.clickUrl = clickUrl;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public int getNewEcpi() {
        return newEcpi;
    }

    public void setNewEcpi(int newEcpi) {
        this.newEcpi = newEcpi;
    }

    public String getSiteHost() {
        return siteHost;
    }

    public void setSiteHost(String siteHost) {
        this.siteHost = siteHost;
    }

    public Listing(int rank, String title, String description, String adultRating, String phoneNumber, int newEcpi, String siteHost, ClickUrl clickUrl) {
        this.rank = rank;
        this.title = title;
        this.description = description;
        this.adultRating = adultRating;
        this.phoneNumber = phoneNumber;
        this.newEcpi = newEcpi;
        this.siteHost = siteHost;
        this.clickUrl = clickUrl;
    }

    public Listing() {
    }
}
