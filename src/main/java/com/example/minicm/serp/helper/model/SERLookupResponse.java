package com.example.minicm.serp.helper.model;

public class SERLookupResponse {
    private Results results;

    public Results getResults() {
        return results;
    }

    public void setResults(Results results) {
        this.results = results;
    }
}
