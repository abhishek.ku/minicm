package com.example.minicm.serp.helper.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class KeywordsRatingSet {
    @JacksonXmlProperty(localName = "keywords", isAttribute = true)
    private String keywords;
    @JacksonXmlProperty(localName = "Market")
    private String market;

    public String getKeywords() {
        return keywords;
    }

    public void setKeywords(String keywords) {
        this.keywords = keywords;
    }

    public String getMarket() {
        return market;
    }

    public void setMarket(String market) {
        this.market = (market != null)? market.trim() : null;
    }

}
