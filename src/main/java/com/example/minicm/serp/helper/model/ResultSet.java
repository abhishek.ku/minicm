package com.example.minicm.serp.helper.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

public class ResultSet {
    @JacksonXmlProperty(localName = "id", isAttribute = true)
    private String id;
    @JacksonXmlProperty(localName = "numResults", isAttribute = true)
    private int numResults;
    @JacksonXmlProperty(localName = "adultRating", isAttribute = true)
    private String adultRating;
    @JacksonXmlProperty(localName = "plaCount", isAttribute = true)
    private int plaCount;
    @JacksonXmlElementWrapper(localName = "Listing", useWrapping = false)
    @JsonProperty("Listing")
    private Listing[] listings;
    @JacksonXmlProperty(localName = "NextArgs")
    private String nextArgs;

    @JacksonXmlProperty(localName = "searchId")
    private String searchId;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getNumResults() {
        return numResults;
    }

    public void setNumResults(int numResults) {
        this.numResults = numResults;
    }

    public String getAdultRating() {
        return adultRating;
    }

    public void setAdultRating(String adultRating) {
        this.adultRating = adultRating;
    }

    public int getPlaCount() {
        return plaCount;
    }

    public void setPlaCount(int plaCount) {
        this.plaCount = plaCount;
    }

    public Listing[] getListings() {
        return listings;
    }

    public void setListings(Listing[] listings) {
        this.listings = listings;
    }

    public String getNextArgs() {
        return nextArgs;
    }

    public void setNextArgs(String nextArgs) {
        this.nextArgs = (nextArgs != null)? nextArgs.trim() : null;
    }

    public ResultSet() {
    }
}
