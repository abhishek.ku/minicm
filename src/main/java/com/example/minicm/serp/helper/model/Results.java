package com.example.minicm.serp.helper.model;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

@JacksonXmlRootElement(localName = "Results")
public class Results {
    @JacksonXmlProperty(localName = "noNamespaceSchemaLocation", isAttribute = true)
    private String noNamespaceSchemaLocation;
    @JacksonXmlProperty(localName = "KeywordsRatingSet")
    private KeywordsRatingSet keywordsRatingSet;
    @JacksonXmlProperty(localName = "SearchID")
    private String searchId;
    @JacksonXmlProperty(localName = "ImpressionGUID")
    private String impressionGuid;
    @JacksonXmlProperty(localName = "ResultSet")
    private ResultSet resultSet;

    public String getNoNamespaceSchemaLocation() {
        return noNamespaceSchemaLocation;
    }

    public void setNoNamespaceSchemaLocation(String noNamespaceSchemaLocation) {
        this.noNamespaceSchemaLocation = noNamespaceSchemaLocation;
    }

    public KeywordsRatingSet getKeywordsRatingSet() {
        return keywordsRatingSet;
    }

    public void setKeywordsRatingSet(KeywordsRatingSet keywordsRatingSet) {
        this.keywordsRatingSet = keywordsRatingSet;
    }

    public String getSearchId() {
        return searchId;
    }

    public void setSearchId(String searchId) {
        this.searchId = searchId;
    }

    public String getImpressionGuid() {
        return impressionGuid;
    }

    public void setImpressionGuid(String impressionGuid) {
        this.impressionGuid = impressionGuid;
    }

    public ResultSet getResultSet() {
        return resultSet;
    }

    public void setResultSet(ResultSet resultSet) {
        this.resultSet = resultSet;
    }
}
