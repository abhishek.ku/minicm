package com.example.minicm.serp.helper;

import com.example.minicm.serp.helper.model.SERLookupRequest;
import com.example.minicm.serp.helper.model.SERLookupResponse;

import java.io.IOException;

public interface SERLookupHelper {
    SERLookupResponse performSERLookup(SERLookupRequest lookupRequest);
}
