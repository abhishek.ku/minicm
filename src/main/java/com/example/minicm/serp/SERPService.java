package com.example.minicm.serp;

import com.example.minicm.aspect.timeLogging.TimeLogged;
import com.example.minicm.monitoring.MonitoringService;
import com.example.minicm.monitoring.model.AdClickModel;
import com.example.minicm.monitoring.model.KeywordClickModel;
import com.example.minicm.serp.helper.SERLookupHelper;
import com.example.minicm.serp.helper.model.ResultSet;
import com.example.minicm.serp.helper.model.Results;
import com.example.minicm.serp.helper.model.SERLookupRequest;
import com.example.minicm.serp.helper.model.SERLookupResponse;
import com.example.minicm.serp.model.AdClickRequest;
import com.example.minicm.serp.model.SERPResponse;
import com.example.minicm.targeting.TargetingService;
import com.example.minicm.targeting.model.SERStyles;
import com.example.minicm.targeting.model.TargetingServiceRequest;
import com.example.minicm.utility.ip.IPUtility;
import com.example.minicm.utility.ip.model.IPUtilityDTO;
import com.example.minicm.utility.location.LocationUtility;
import com.example.minicm.utility.location.model.Location;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Map;
import java.util.concurrent.ExecutorService;

@Service
public class SERPService {
    private static final Logger LOG = LoggerFactory.getLogger(SERPService.class);
    private SERLookupHelper serLookupHelper;
    private LocationUtility locationUtility;
    private IPUtility ipUtility;
    private MonitoringService monitoringService;
    private ExecutorService executorService;
    private TargetingService targetingService;

    @Autowired
    public void setDependencies(SERLookupHelper serLookupHelper, LocationUtility locationUtility,
                                IPUtility ipUtility, MonitoringService monitoringService,
                                ExecutorService executorService, TargetingService targetingService) {
        this.serLookupHelper = serLookupHelper;
        this.locationUtility = locationUtility;
        this.ipUtility = ipUtility;
        this.monitoringService = monitoringService;
        this.executorService = executorService;
        this.targetingService = targetingService;
    }

    @TimeLogged
    public SERPResponse getSEResults(Map<String, String[]> parameterMap, IPUtilityDTO ipUtilityDTO) {
        LOG.info("getSEResults()");
        LOG.debug(parameterMap.toString());
        LOG.debug(ipUtilityDTO.toString());

        SERPResponse response = new SERPResponse();

        String[] ck = parameterMap.get("ck");
        String clickedKeyword = (ck != null && ck.length > 0)? ck[0]: null;

        String[] u = parameterMap.get("u");
        String url = (u != null && u.length > 0)? u[0]: null;

        String[] r = parameterMap.get("r");
        String referer = (r != null && r.length > 0)? r[0]: null;

        String[] o = parameterMap.get("o");
        String os = (o != null && o.length > 0)? o[0]: null;

        String[] b = parameterMap.get("b");
        String browser = (b != null && b.length > 0)? b[0]: null;

        String[] cid = parameterMap.get("cid");
        String customerId = (cid != null && cid.length > 0)? cid[0]: null;

        String[] atid = parameterMap.get("atid");
        String adTagId = (atid != null && atid.length > 0)? atid[0]: null;

        // Get user's ip
        LOG.info("fetching user's ip");
        String ip = ipUtility.getClientIpAddress(ipUtilityDTO);
        LOG.debug("ip: " + ip);

        // Perform lookup for search engine results
        SERLookupRequest serLookupRequest = new SERLookupRequest();
        serLookupRequest.setClickedKeyword(clickedKeyword);
        serLookupRequest.setIp(ip);
        serLookupRequest.setServeURL(url);

        LOG.info("performing ser lookup");
        SERLookupResponse serLookupResponse = serLookupHelper.performSERLookup(serLookupRequest);

        if (serLookupResponse != null) {
            Results results = serLookupResponse.getResults();
            if (results != null) {
                ResultSet rs = results.getResultSet();
                if (rs != null && rs.getListings() != null) {
                    response.setListings(rs.getListings());
                }
            }
        }

        // Get user's location
        LOG.info("fetching user's location");
        Location userLocation = locationUtility.getLocationFromIP(ip);
        LOG.debug(userLocation.toString());

        // Add keyword click record
        LOG.info("adding keyword click record");
        addKeywordClickRecord(clickedKeyword, adTagId, customerId, browser, os, url, referer, ip, userLocation);

        // Get styles
        LOG.info("fetching styles");
        response.setStyles(getStyles(customerId, browser, os, userLocation, adTagId, url));
        return response;
    }

    @TimeLogged
    public void addAdClickRecord(AdClickRequest request, IPUtilityDTO ipUtilityDTO) {
        LOG.info("addAdClickRecord");
        LOG.debug(request.toString());
        LOG.debug(ipUtilityDTO.toString());

        LOG.info("fetching user's ip");
        String ip = ipUtility.getClientIpAddress(ipUtilityDTO);
        LOG.debug("ip: " + ip);

        final AdClickModel model = new AdClickModel();

        model.setAdTitle(request.getAdTitle());
        model.setCustomerId(request.getCustomerId());
        model.setUrl(request.getUrl());
        model.setReferer(request.getReferer());
        model.setBrowser(request.getBrowser());
        model.setOs(request.getOs());
        model.setUserIP(ip);
        model.setLocation(locationUtility.getLocationFromIP(ip));

        executorService.execute(()-> monitoringService.addAdClickRecord(model));
    }

    private SERStyles getStyles(String customerId, String browser, String os, Location location, String adTagId, String pageURL) {
        TargetingServiceRequest targetingServiceRequest = new TargetingServiceRequest();
        targetingServiceRequest.setCustomerId(customerId);
        targetingServiceRequest.setBrowser(browser);
        targetingServiceRequest.setOs(os);
        targetingServiceRequest.setLocation(location);
        targetingServiceRequest.setAdTagId(adTagId);

        try {
            URL url = new URL(pageURL);
            targetingServiceRequest.setPattern(url.getPath().replaceFirst("/", "").replaceFirst("\\..*", "").toLowerCase());
        } catch (MalformedURLException e) {
            LOG.error("URL parsing failed", e);
            targetingServiceRequest.setPattern("*");
        }

        return targetingService.getSERAdStyles(targetingServiceRequest);
    }

    private void addKeywordClickRecord(
            String clickedKeyword, String adTagHTMLId, String customerId, String browser, String os, String url,
            String referer, String ip, Location location) {
        final KeywordClickModel model = new KeywordClickModel();
        model.setClickedKeyword(clickedKeyword);
        model.setAdTagHTMLId(adTagHTMLId);
        model.setCustomerId(customerId);
        model.setUrl(url);
        model.setReferer(referer);
        model.setUserIP(ip);
        model.setLocation(location);
        model.setBrowser(browser);
        model.setOs(os);

        executorService.execute(()-> monitoringService.addKeywordClickRecord(model));
    }
}
