package com.example.minicm.serp;

import com.example.minicm.aspect.timeLogging.TimeLogged;
import com.example.minicm.serp.model.AdClickRequest;
import com.example.minicm.serp.model.SERPResponse;
import com.example.minicm.utility.ip.model.IPUtilityDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

@Controller
public class SERPController {
    private static final Logger LOG = LoggerFactory.getLogger(SERPController.class);
    private SERPService serpService;

    @Autowired
    public void setDependencies(SERPService serpService) {
        this.serpService = serpService;
    }

    @GetMapping(value = "seResults", produces = "application/json")
    @TimeLogged
    public @ResponseBody() SERPResponse getSERPResults(HttpServletRequest httpServletRequest) {
        LOG.info("getSERPResults()");

        Map<String, String> headerMap = new HashMap<>();

        Enumeration<String> headerNames = httpServletRequest.getHeaderNames();
        while(headerNames.hasMoreElements()) {
            String headerName = headerNames.nextElement();
            headerMap.put(headerName, httpServletRequest.getHeader(headerName));
        }

        IPUtilityDTO ipUtilityDTO = new IPUtilityDTO();
        ipUtilityDTO.setHeaderMap(headerMap);
        ipUtilityDTO.setRemoteAddress(httpServletRequest.getRemoteAddr());

        return serpService.getSEResults(httpServletRequest.getParameterMap(), ipUtilityDTO);
    }

    @PostMapping("serpAdClick")
    @TimeLogged
    public @ResponseBody void onSerpAdClick(@RequestBody AdClickRequest adClickRequest, HttpServletRequest httpServletRequest) {
        Map<String, String> headerMap = new HashMap<>();

        Enumeration<String> headerNames = httpServletRequest.getHeaderNames();
        while(headerNames.hasMoreElements()) {
            String headerName = headerNames.nextElement();
            headerMap.put(headerName, httpServletRequest.getHeader(headerName));
        }

        IPUtilityDTO ipUtilityDTO = new IPUtilityDTO();
        ipUtilityDTO.setHeaderMap(headerMap);
        ipUtilityDTO.setRemoteAddress(httpServletRequest.getRemoteAddr());

        serpService.addAdClickRecord(adClickRequest, ipUtilityDTO);
    }
}
