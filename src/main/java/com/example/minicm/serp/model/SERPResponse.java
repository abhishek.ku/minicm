package com.example.minicm.serp.model;

import com.example.minicm.serp.helper.model.Listing;
import com.example.minicm.targeting.model.SERStyles;
import com.fasterxml.jackson.annotation.JsonProperty;

public class SERPResponse {
    @JsonProperty("l")
    private Listing[] listings;
    @JsonProperty("s")
    private SERStyles styles;

    public Listing[] getListings() {
        return listings;
    }

    public void setListings(Listing[] listings) {
        this.listings = listings;
    }

    public SERStyles getStyles() {
        return styles;
    }

    public void setStyles(SERStyles styles) {
        this.styles = styles;
    }
}
