package com.example.minicm.serp.model;

public class AdClickRequest {
    private String adTitle;
    private String customerId;
    private String browser;
    private String os;
    private String url;
    private String referer;

    public String getAdTitle() {
        return adTitle;
    }

    public void setAdTitle(String adTitle) {
        this.adTitle = adTitle;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getBrowser() {
        return browser;
    }

    public void setBrowser(String browser) {
        this.browser = browser;
    }

    public String getOs() {
        return os;
    }

    public void setOs(String os) {
        this.os = os;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getReferer() {
        return referer;
    }

    public void setReferer(String referer) {
        this.referer = referer;
    }

    @Override
    public String toString() {
        return "AdClickRequest{" +
                "adTitle='" + adTitle + '\'' +
                ", customerId='" + customerId + '\'' +
                ", browser='" + browser + '\'' +
                ", os='" + os + '\'' +
                ", url='" + url + '\'' +
                ", referer='" + referer + '\'' +
                '}';
    }
}
