package com.example.minicm;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MiniCMApplication {
    public static void main(String[] args) {
        SpringApplication.run(MiniCMApplication.class, args);
    }


}
