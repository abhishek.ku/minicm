package com.example.minicm.aspect.timeLogging;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;

@Aspect
@Configuration
public class TimeLoggingAspect {
    private static final Logger LOG = LoggerFactory.getLogger(TimeLoggingAspect.class);

    @Around("@annotation(com.example.minicm.aspect.timeLogging.TimeLogged)")
    public Object logExecutionTime(ProceedingJoinPoint pjp) throws Throwable {
        long startTime = System.currentTimeMillis();

        Object returnValue = pjp.proceed();

        long endTime = System.currentTimeMillis();
        Signature signature = pjp.getSignature();
        LOG.info(signature.getDeclaringTypeName() + "." + signature.getName() + " ended in " + (endTime - startTime) + " ms");
        return returnValue;
    }
}
