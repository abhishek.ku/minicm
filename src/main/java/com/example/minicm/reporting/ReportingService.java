package com.example.minicm.reporting;

import com.example.minicm.reporting.model.KeywordRenderStats;
import com.example.minicm.reporting.model.ReportingRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class ReportingService {
    private static final Logger LOG = LoggerFactory.getLogger(ReportingService.class);
    private ReportingRepo reportingRepo;

    @Autowired
    public void setDependencies(ReportingRepo reportingRepo) {
        this.reportingRepo = reportingRepo;
    }

    public List<Map<String, Object>> getKeywordRenderStats(ReportingRequest request) {
        return reportingRepo.getKeywordRenderStats(request);
    }

    public List<Map<String, Object>> getKeywordClickStats(ReportingRequest request) {
        return reportingRepo.getKeywordClickStats(request);
    }

    public List<Map<String, Object>> getAdClickStats(ReportingRequest request) {
        return reportingRepo.getAdClickStats(request);
    }

    public List<Map<String, Object>> getPageVisitStats(ReportingRequest request) {
        return reportingRepo.getPageVisitStats(request);
    }
}
