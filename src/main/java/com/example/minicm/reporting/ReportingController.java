package com.example.minicm.reporting;

import com.example.minicm.reporting.model.KeywordRenderStats;
import com.example.minicm.reporting.model.ReportingRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;
import java.util.Map;

@Controller
public class ReportingController {
    private static final Logger LOG = LoggerFactory.getLogger(ReportingController.class);
    private ReportingService reportingService;

    @Autowired
    public void setDependencies(ReportingService reportingService) {
        this.reportingService = reportingService;
    }

    @PostMapping("getPageVisitStats")
    public @ResponseBody List<Map<String, Object>> getPageVisitStats(@RequestBody ReportingRequest request) {
        LOG.info("getPageVisitStats()");
        LOG.debug(request.toString());

        return reportingService.getPageVisitStats(request);
    }

    @PostMapping("getKeywordRenderStats")
    public @ResponseBody List<Map<String, Object>> getKeywordRenderStats(@RequestBody ReportingRequest request) {
        LOG.info("getKeywordRenderStats()");
        LOG.debug(request.toString());

        return reportingService.getKeywordRenderStats(request);
    }

    @PostMapping("getKeywordClickStats")
    public @ResponseBody List<Map<String, Object>> getKeywordClickStats(@RequestBody ReportingRequest request) {
        LOG.info("getKeywordClickStats()");
        LOG.debug(request.toString());

        return reportingService.getKeywordClickStats(request);
    }

    @PostMapping("getAdClickStats")
    public @ResponseBody List<Map<String, Object>> getAdClickStats(@RequestBody ReportingRequest request) {
        LOG.info("getAdClickStats()");
        LOG.debug(request.toString());

        return reportingService.getAdClickStats(request);
    }
}
