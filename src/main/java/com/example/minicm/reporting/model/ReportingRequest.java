package com.example.minicm.reporting.model;

public class ReportingRequest {
    private String customerID;
    private String keyword;
    private String adTag;
    private String pageURL;
    private String browser;
    private String os;
    private String country;
    private String state;

    public String getCustomerID() {
        return customerID;
    }

    public void setCustomerID(String customerID) {
        this.customerID = customerID;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public String getAdTag() {
        return adTag;
    }

    public void setAdTag(String adTag) {
        this.adTag = adTag;
    }

    public String getPageURL() {
        return pageURL;
    }

    public void setPageURL(String pageURL) {
        this.pageURL = pageURL;
    }

    public String getBrowser() {
        return browser;
    }

    public void setBrowser(String browser) {
        this.browser = browser;
    }

    public String getOs() {
        return os;
    }

    public void setOs(String os) {
        this.os = os;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    @Override
    public String toString() {
        return "ReportingRequest{" +
                "customerID='" + customerID + '\'' +
                ", keyword='" + keyword + '\'' +
                ", adTag='" + adTag + '\'' +
                ", pageURL='" + pageURL + '\'' +
                ", browser='" + browser + '\'' +
                ", os='" + os + '\'' +
                ", country='" + country + '\'' +
                ", state='" + state + '\'' +
                '}';
    }
}
