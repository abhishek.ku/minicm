package com.example.minicm.reporting;

import com.example.minicm.reporting.model.KeywordRenderStats;
import com.example.minicm.reporting.model.ReportingRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Repository
public class ReportingRepo {
    private static final Logger LOG = LoggerFactory.getLogger(ReportingRepo.class);
    private NamedParameterJdbcTemplate jdbcTemplate;

    private String selectKeywordRenderStatsSQL = "select kd.text keyword, dd.customer_id customer_id, atd.html_tag_id ad_tag, pd.url page_url, pd2.url referer, bd.name browser, od.name os, cd.name country, cs.state_name state, krr.timestamp from keyword_render_record krr join keyword_details kd join page_visit_record pvr join domain_details dd join page_details pd join page_details pd2 join ad_tag_details atd join browser_details bd join os_details od join country_states cs join country_details cd on krr.page_visit_id = pvr.id and krr.keyword_id = kd.id and pvr.page_id = pd.id and pvr.referer_id = pd2.id and pvr.browser_id = bd.id and pvr.os_id = od.id and pvr.country_state_id = cs.id and cs.country_id = cd.id and pvr.domain_id = dd.id and krr.ad_tag_id = atd.id where pvr.domain_id = (select id from domain_details where customer_id = :customerID) ";
    private String selectKeywordClickStatsSQL = "select kd.text keyword, dd.customer_id, atd.html_tag_id ad_tag, pd.url page_url, pd2.url referer, bd.name browser, od.name os, cd.name country, cs.state_name state, kcr.timestamp from keyword_click_record kcr join keyword_details kd join domain_details dd join page_details pd join page_details pd2 join ad_tag_details atd join browser_details bd join os_details od join country_states cs join country_details cd on kcr.keyword_id = kd.id and kcr.page_id = pd.id and kcr.referer_id = pd2.id and kcr.browser_id = bd.id and kcr.os_id = od.id and kcr.country_state_id = cs.id and cs.country_id = cd.id and kcr.domain_id = dd.id and kcr.ad_tag_id = atd.id where kcr.domain_id = (select id from domain_details where customer_id = :customerID) ";
    private String selectAdClickStatsSQL = "select sd.ad_title ad_title, dd.customer_id, pd.url page_url, pd2.url referer, bd.name browser, od.name os, cd.name country, cs.state_name state, acr.timestamp from ad_click_record acr join domain_details dd join page_details pd join page_details pd2 join ser_ad_details sd join browser_details bd join os_details od join country_states cs join country_details cd on acr.domain_id = dd.id and acr.page_id = pd.id and acr.referer_id = pd2.id and acr.browser_id = bd.id and acr.os_id = od.id and acr.country_state_id = cs.id and cs.country_id = cd.id and acr.ad_id = sd.id where acr.domain_id = (select id from domain_details where customer_id = :customerID)";
    private String selectPageVisitStatsSQL = "select dd.customer_id, pd.url page_url, pd2.url referer, bd.name browser, od.name os, cd.name country, cs.state_name state, pvr.timestamp from page_visit_record pvr join domain_details dd join page_details pd join page_details pd2 join browser_details bd join os_details od join country_states cs join country_details cd on pvr.domain_id = dd.id and pvr.page_id = pd.id and pvr.referer_id = pd2.id and pvr.browser_id = bd.id and pvr.os_id = od.id and pvr.country_state_id = cs.id and cs.country_id = cd.id where pvr.domain_id = (select id from domain_details where customer_id = :customerID)";

    @Autowired
    public void setDependencies(NamedParameterJdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public List<Map<String, Object>> getKeywordRenderStats(ReportingRequest request) {
        LOG.info("getKeywordRenderStats()");
        LOG.debug(request.toString());

        String query = selectKeywordRenderStatsSQL;
        MapSqlParameterSource parameterSource = new MapSqlParameterSource();
        parameterSource.addValue("customerID", request.getCustomerID());

        if (request.getAdTag() != null) {
            query += " and atd.html_tag_id = :adTag ";
            parameterSource.addValue("adTag", request.getAdTag());
        } if (request.getPageURL() != null) {
            query += " and pd.url = :pageURL ";
            parameterSource.addValue("pageURL", request.getPageURL());
        } if (request.getBrowser() != null) {
            query += " and bd.name = :browser ";
            parameterSource.addValue("browser", request.getBrowser());
        } if (request.getOs() != null) {
            query += " and od.name = :os ";
            parameterSource.addValue("os", request.getOs());
        } if (request.getCountry() != null) {
            query += " and cd.name = :country ";
            parameterSource.addValue("country", request.getCountry());
        } if (request.getState() != null) {
            query += " and cs.state_name = :state ";
            parameterSource.addValue("state", request.getState());
        }

        return jdbcTemplate.queryForList(query, parameterSource);
    }

    public List<Map<String, Object>> getKeywordClickStats(ReportingRequest request) {
        LOG.info("getKeywordClickStats()");
        LOG.debug(request.toString());

        String query = selectKeywordClickStatsSQL;
        MapSqlParameterSource parameterSource = new MapSqlParameterSource();
        parameterSource.addValue("customerID", request.getCustomerID());

        if (request.getAdTag() != null) {
            query += " and atd.html_tag_id = :adTag ";
            parameterSource.addValue("adTag", request.getAdTag());
        } if (request.getKeyword() != null) {
            query += " and kd.text = :keyword ";
            parameterSource.addValue("keyword", request.getKeyword());
        } if (request.getPageURL() != null) {
            query += " and pd.url = :pageURL ";
            parameterSource.addValue("pageURL", request.getPageURL());
        } if (request.getBrowser() != null) {
            query += " and bd.name = :browser ";
            parameterSource.addValue("browser", request.getBrowser());
        } if (request.getOs() != null) {
            query += " and od.name = :os ";
            parameterSource.addValue("os", request.getOs());
        } if (request.getCountry() != null) {
            query += " and cd.name = :country ";
            parameterSource.addValue("country", request.getCountry());
        } if (request.getState() != null) {
            query += " and cs.state_name = :state ";
            parameterSource.addValue("state", request.getState());
        }

        return jdbcTemplate.queryForList(query, parameterSource);
    }

    public List<Map<String, Object>> getAdClickStats(ReportingRequest request) {
        LOG.info("getKeywordClickStats()");
        LOG.debug(request.toString());

        String query = selectAdClickStatsSQL;
        MapSqlParameterSource parameterSource = new MapSqlParameterSource();
        parameterSource.addValue("customerID", request.getCustomerID());

        if (request.getPageURL() != null) {
            query += " and pd.url = :pageURL ";
            parameterSource.addValue("pageURL", request.getPageURL());
        } if (request.getBrowser() != null) {
            query += " and bd.name = :browser ";
            parameterSource.addValue("browser", request.getBrowser());
        } if (request.getOs() != null) {
            query += " and od.name = :os ";
            parameterSource.addValue("os", request.getOs());
        } if (request.getCountry() != null) {
            query += " and cd.name = :country ";
            parameterSource.addValue("country", request.getCountry());
        } if (request.getState() != null) {
            query += " and cs.state_name = :state ";
            parameterSource.addValue("state", request.getState());
        }

        return jdbcTemplate.queryForList(query, parameterSource);
    }

    public List<Map<String, Object>> getPageVisitStats(ReportingRequest request) {
        LOG.info("getPageVisitStats()");
        LOG.debug(request.toString());

        String query = selectPageVisitStatsSQL;
        MapSqlParameterSource parameterSource = new MapSqlParameterSource();
        parameterSource.addValue("customerID", request.getCustomerID());

        if (request.getPageURL() != null) {
            query += " and pd.url = :pageURL ";
            parameterSource.addValue("pageURL", request.getPageURL());
        } if (request.getBrowser() != null) {
            query += " and bd.name = :browser ";
            parameterSource.addValue("browser", request.getBrowser());
        } if (request.getOs() != null) {
            query += " and od.name = :os ";
            parameterSource.addValue("os", request.getOs());
        } if (request.getCountry() != null) {
            query += " and cd.name = :country ";
            parameterSource.addValue("country", request.getCountry());
        } if (request.getState() != null) {
            query += " and cs.state_name = :state ";
            parameterSource.addValue("state", request.getState());
        }

        return jdbcTemplate.queryForList(query, parameterSource);
    }
}
