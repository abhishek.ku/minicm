package com.example.minicm.monitoring.model;

public class KeywordRenderModel {
    private AdTagAndKeywords adTagAndKeywords;
    private int pageVisitID;

    public AdTagAndKeywords getAdTagAndKeywords() {
        return adTagAndKeywords;
    }

    public void setAdTagAndKeywords(AdTagAndKeywords adTagAndKeywords) {
        this.adTagAndKeywords = adTagAndKeywords;
    }

    public int getPageVisitID() {
        return pageVisitID;
    }

    public void setPageVisitID(int pageVisitID) {
        this.pageVisitID = pageVisitID;
    }

    @Override
    public String toString() {
        return "KeywordRenderModel{" +
                "adTagAndKeywords=" + adTagAndKeywords +
                ", pageVisitID=" + pageVisitID +
                '}';
    }
}
