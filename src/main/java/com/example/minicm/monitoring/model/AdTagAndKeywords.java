package com.example.minicm.monitoring.model;

import java.util.List;

public class AdTagAndKeywords {
    private String adTagHTMLId;
    private List<String> displayedKeywords;

    public String getAdTagHTMLId() {
        return adTagHTMLId;
    }

    public void setAdTagHTMLId(String adTagHTMLId) {
        this.adTagHTMLId = adTagHTMLId;
    }

    public List<String> getDisplayedKeywords() {
        return displayedKeywords;
    }

    public void setDisplayedKeywords(List<String> displayedKeywords) {
        this.displayedKeywords = displayedKeywords;
    }
}
