package com.example.minicm.monitoring.model;

public class IDSet {
    private int domainId;
    private int adTagId;
    private int browserId;
    private int osId;
    private int pageId;
    private int locationId;
    private int refererId;

    public int getDomainId() {
        return domainId;
    }

    public void setDomainId(int domainId) {
        this.domainId = domainId;
    }

    public int getAdTagId() {
        return adTagId;
    }

    public void setAdTagId(int adTagId) {
        this.adTagId = adTagId;
    }

    public int getBrowserId() {
        return browserId;
    }

    public void setBrowserId(int browserId) {
        this.browserId = browserId;
    }

    public int getOsId() {
        return osId;
    }

    public void setOsId(int osId) {
        this.osId = osId;
    }

    public int getPageId() {
        return pageId;
    }

    public void setPageId(int pageId) {
        this.pageId = pageId;
    }

    public int getLocationId() {
        return locationId;
    }

    public void setLocationId(int locationId) {
        this.locationId = locationId;
    }

    public int getRefererId() {
        return refererId;
    }

    public void setRefererId(int refererId) {
        this.refererId = refererId;
    }

    @Override
    public String toString() {
        return "IDSet{" +
                "domainId=" + domainId +
                ", adTagId=" + adTagId +
                ", browserId=" + browserId +
                ", osId=" + osId +
                ", pageId=" + pageId +
                ", locationId=" + locationId +
                ", refererId=" + refererId +
                '}';
    }
}
