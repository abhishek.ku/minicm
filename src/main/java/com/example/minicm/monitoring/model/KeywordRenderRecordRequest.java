package com.example.minicm.monitoring.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class KeywordRenderRecordRequest {
    private List<AdTagAndKeywords> adTagsAndDisplayedKeywords;
    private int pageVisitID;

    public List<AdTagAndKeywords> getAdTagsAndDisplayedKeywords() {
        return adTagsAndDisplayedKeywords;
    }

    public void setAdTagsAndDisplayedKeywords(List<AdTagAndKeywords> adTagsAndDisplayedKeywords) {
        this.adTagsAndDisplayedKeywords = adTagsAndDisplayedKeywords;
    }

    public int getPageVisitID() {
        return pageVisitID;
    }

    public void setPageVisitID(int pageVisitID) {
        this.pageVisitID = pageVisitID;
    }

    @Override
    public String toString() {
        return "KeywordRenderRecordRequest{" +
                "adTagsAndDisplayedKeywords=" + adTagsAndDisplayedKeywords +
                ", pageVisitID=" + pageVisitID +
                '}';
    }
}
