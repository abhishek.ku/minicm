package com.example.minicm.monitoring.model;

import com.example.minicm.utility.location.model.Location;

public class AdClickModel {
    private String adTitle;
    private String customerId;
    private String browser;
    private String os;
    private String url;
    private Location location;
    private String referer;
    private String userIP;

    public String getAdTitle() {
        return adTitle;
    }

    public void setAdTitle(String adTitle) {
        this.adTitle = adTitle;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getBrowser() {
        return browser;
    }

    public void setBrowser(String browser) {
        this.browser = browser;
    }

    public String getOs() {
        return os;
    }

    public void setOs(String os) {
        this.os = os;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public String getReferer() {
        return referer;
    }

    public void setReferer(String referer) {
        this.referer = referer;
    }

    public String getUserIP() {
        return userIP;
    }

    public void setUserIP(String userIP) {
        this.userIP = userIP;
    }

    @Override
    public String toString() {
        return "AdClickModel{" +
                "adTitle='" + adTitle + '\'' +
                ", customerId='" + customerId + '\'' +
                ", browser='" + browser + '\'' +
                ", os='" + os + '\'' +
                ", url='" + url + '\'' +
                ", location=" + location.toString() +
                ", referer='" + referer + '\'' +
                ", userIP='" + userIP + '\'' +
                '}';
    }
}
