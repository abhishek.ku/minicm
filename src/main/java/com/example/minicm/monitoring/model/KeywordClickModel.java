package com.example.minicm.monitoring.model;

import com.example.minicm.utility.location.model.Location;

public class KeywordClickModel {
    private String clickedKeyword;
    private String customerId;
    private String adTagHTMLId;
    private String browser;
    private String os;
    private String url;
    private Location location;
    private String referer;
    private String userIP;

    public String getClickedKeyword() {
        return clickedKeyword;
    }

    public void setClickedKeyword(String clickedKeyword) {
        this.clickedKeyword = clickedKeyword;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getAdTagHTMLId() {
        return adTagHTMLId;
    }

    public void setAdTagHTMLId(String adTagHTMLId) {
        this.adTagHTMLId = adTagHTMLId;
    }

    public String getBrowser() {
        return browser;
    }

    public void setBrowser(String browser) {
        this.browser = browser;
    }

    public String getOs() {
        return os;
    }

    public void setOs(String os) {
        this.os = os;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public String getReferer() {
        return referer;
    }

    public void setReferer(String referer) {
        this.referer = referer;
    }

    public String getUserIP() {
        return userIP;
    }

    public void setUserIP(String userIP) {
        this.userIP = userIP;
    }

    @Override
    public String toString() {
        return "KeywordClickModel{" +
                "clickedKeyword='" + clickedKeyword + '\'' +
                ", customerId='" + customerId + '\'' +
                ", adTagHTMLId='" + adTagHTMLId + '\'' +
                ", browser='" + browser + '\'' +
                ", os='" + os + '\'' +
                ", url='" + url + '\'' +
                ", location=" + location.toString() +
                ", referer='" + referer + '\'' +
                ", userIP='" + userIP + '\'' +
                '}';
    }
}
