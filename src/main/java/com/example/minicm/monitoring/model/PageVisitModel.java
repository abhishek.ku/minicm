package com.example.minicm.monitoring.model;

import com.example.minicm.utility.location.model.Location;

public class PageVisitModel {
    private String userIP;
    private Location location;
    private String customerId;
    private String pageURL;
    private String referer;
    private String browser;
    private String os;

    public String getUserIP() {
        return userIP;
    }

    public void setUserIP(String userIP) {
        this.userIP = userIP;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getPageURL() {
        return pageURL;
    }

    public void setPageURL(String pageURL) {
        this.pageURL = pageURL;
    }

    public String getReferer() {
        return referer;
    }

    public void setReferer(String referer) {
        this.referer = referer;
    }

    public String getBrowser() {
        return browser;
    }

    public void setBrowser(String browser) {
        this.browser = browser;
    }

    public String getOs() {
        return os;
    }

    public void setOs(String os) {
        this.os = os;
    }

    @Override
    public String toString() {
        return "PageVisitModel{" +
                "userIP='" + userIP + '\'' +
                ", location=" + location.toString() +
                ", customerId='" + customerId + '\'' +
                ", pageURL='" + pageURL + '\'' +
                ", referer='" + referer + '\'' +
                ", browser='" + browser + '\'' +
                ", os='" + os + '\'' +
                '}';
    }
}
