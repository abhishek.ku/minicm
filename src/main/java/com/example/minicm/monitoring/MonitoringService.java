package com.example.minicm.monitoring;

import com.example.minicm.aspect.timeLogging.TimeLogged;
import com.example.minicm.monitoring.model.*;
import com.example.minicm.utility.ip.IPUtility;
import com.example.minicm.utility.ip.model.IPUtilityDTO;
import com.example.minicm.utility.location.LocationUtility;
import com.example.minicm.utility.location.model.Location;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MonitoringService {
    private static final Logger LOG = LoggerFactory.getLogger(MonitoringService.class);
    private MonitoringRepo monitoringRepo;
    private IPUtility ipUtility;
    private LocationUtility locationUtility;

    @Autowired
    public void setDependencies(
            MonitoringRepo monitoringRepo, IPUtility ipUtility, LocationUtility locationUtility) {
        this.monitoringRepo = monitoringRepo;
        this.ipUtility = ipUtility;
        this.locationUtility = locationUtility;
    }

    @TimeLogged
    public void addKeywordRenderRecord(KeywordRenderRecordRequest recordRequest, IPUtilityDTO ipUtilityDTO) {
        LOG.info("addKeywordRenderRecord()");
        LOG.debug(recordRequest.toString());
        LOG.debug(ipUtilityDTO.toString());

        LOG.info("fetching user's ip");
        String userIpAddress = ipUtility.getClientIpAddress(ipUtilityDTO);
        LOG.debug("user ip: " + userIpAddress);
        LOG.info("fetching user location");
        Location userLocation = locationUtility.getLocationFromIP(userIpAddress);
        LOG.debug("user location: " + userLocation.toString());

        recordRequest.getAdTagsAndDisplayedKeywords().forEach(e -> {
            KeywordRenderModel model = new KeywordRenderModel();
            model.setAdTagAndKeywords(e);
            model.setPageVisitID(recordRequest.getPageVisitID());

            monitoringRepo.insertKeywordRenderRecord(model);
        });
    }

    @TimeLogged
    public void addKeywordClickRecord(KeywordClickModel model) {
        LOG.info("addKeywordClickRecord()");
        LOG.debug(model.toString());

        monitoringRepo.insertKeywordClickRecord(model);
    }

    @TimeLogged
    public void addAdClickRecord(AdClickModel model) {
        LOG.info("addAdClickRecord()");
        LOG.debug(model.toString());

        monitoringRepo.insertAdClickRecord(model);
    }

    @TimeLogged
    public int addPageVisitRecord(PageVisitRecordRequest recordRequest, IPUtilityDTO ipUtilityDTO) {
        LOG.info("addPageVisitRecord()");
        LOG.debug(recordRequest.toString());
        LOG.debug(ipUtilityDTO.toString());

        LOG.info("fetching user's ip");
        String userIpAddress = ipUtility.getClientIpAddress(ipUtilityDTO);
        LOG.debug("user ip: " + userIpAddress);
        LOG.info("fetching user location");
        Location userLocation = locationUtility.getLocationFromIP(userIpAddress);
        LOG.debug("user location: " + userLocation.toString());

        PageVisitModel model = new PageVisitModel();
        model.setCustomerId(recordRequest.getCustomerId());
        model.setPageURL(recordRequest.getPageURL());
        model.setReferer(recordRequest.getReferer());
        model.setBrowser(recordRequest.getBrowser());
        model.setOs(recordRequest.getOs());
        model.setUserIP(userIpAddress);
        model.setLocation(userLocation);

        return monitoringRepo.insertPageVisitRecord(model);
    }
}
