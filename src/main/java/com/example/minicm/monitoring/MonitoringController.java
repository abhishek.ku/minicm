package com.example.minicm.monitoring;

import com.example.minicm.aspect.timeLogging.TimeLogged;
import com.example.minicm.monitoring.model.KeywordRenderRecordRequest;
import com.example.minicm.monitoring.model.PageVisitRecordRequest;
import com.example.minicm.utility.ip.model.IPUtilityDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

@Controller
public class MonitoringController {
    private static final Logger LOG = LoggerFactory.getLogger(MonitoringController.class);
    private MonitoringService monitoringService;

    @Autowired
    public void setDependencies(MonitoringService monitoringService) {
        this.monitoringService = monitoringService;
    }

    @PostMapping("addKeywordRenderRecord")
    @CrossOrigin(origins = "*")
    @TimeLogged
    public @ResponseBody void addKeywordRenderRecord(@RequestBody KeywordRenderRecordRequest recordRequest, HttpServletRequest servletRequest) {
        LOG.info("addKeywordRenderRecord()");
        LOG.debug(recordRequest.toString());

        Map<String, String> headerMap = new HashMap<>();

        Enumeration<String> headerNames = servletRequest.getHeaderNames();
        while(headerNames.hasMoreElements()) {
            String headerName = headerNames.nextElement();
            headerMap.put(headerName, servletRequest.getHeader(headerName));
        }

        IPUtilityDTO ipUtilityDTO = new IPUtilityDTO();
        ipUtilityDTO.setHeaderMap(headerMap);
        ipUtilityDTO.setRemoteAddress(servletRequest.getRemoteAddr());

        monitoringService.addKeywordRenderRecord(recordRequest, ipUtilityDTO);
    }

    @PostMapping("addPageVisitRecord")
    @CrossOrigin(origins = "*")
    @TimeLogged
    public @ResponseBody Map<String, Integer> addPageVisitRecord(@RequestBody PageVisitRecordRequest recordRequest, HttpServletRequest servletRequest) {
        LOG.info("addPageVisitRecord()");
        LOG.debug(recordRequest.toString());

        Map<String, String> headerMap = new HashMap<>();

        Enumeration<String> headerNames = servletRequest.getHeaderNames();
        while(headerNames.hasMoreElements()) {
            String headerName = headerNames.nextElement();
            headerMap.put(headerName, servletRequest.getHeader(headerName));
        }

        IPUtilityDTO ipUtilityDTO = new IPUtilityDTO();
        ipUtilityDTO.setHeaderMap(headerMap);
        ipUtilityDTO.setRemoteAddress(servletRequest.getRemoteAddr());

        int pageVisitID = monitoringService.addPageVisitRecord(recordRequest, ipUtilityDTO);
        Map<String, Integer> response = new HashMap<>();
        response.put("pageVisitID", pageVisitID);
        return response;
    }
}
