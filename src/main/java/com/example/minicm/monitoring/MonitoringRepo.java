package com.example.minicm.monitoring;

import com.example.minicm.aspect.timeLogging.TimeLogged;
import com.example.minicm.monitoring.model.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.Map;

@Repository
public class MonitoringRepo {
    private static final Logger LOG = LoggerFactory.getLogger(MonitoringRepo.class);
    private NamedParameterJdbcTemplate jdbcTemplate;

    private final String keywordRenderInsertSQL = "call sp_add_keyword_render_record(:keywordText, :adTagHTMLId, :pageVisitID)";
    private final String keywordClickInsertSQL = "insert into keyword_click_record (keyword_id, domain_id, browser_id, os_id, page_id, ad_tag_id, country_state_id, referer_id, user_ip) VALUES ((select id from keyword_details where text = :keywordText), (select id from domain_details where customer_id = :customerId), (select id from browser_details where name = :browser), (select id from os_details where name = :os), (select id from page_details where url = :url), (select id from ad_tag_details where html_tag_id = :adTagHTMLId), (select id from country_states where country_id = (select id from country_details where name = :country) and state_name = :state), (select id from page_details where url = :referer), :ip)";
    private final String adClickInsertSQL = "call sp_add_ad_click_record(:adTitle, :browser, :os, :customerId, :country, :state, :url, :referer, :ip)";
    private final String pageVisitInsertSQL = "call sp_add_page_visit_record(:browser, :os, :customerId, :country, :state, :url, :referer, :ip)";

    @Autowired
    public void setDependencies(NamedParameterJdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @TimeLogged
    public int[] insertKeywordRenderRecord(KeywordRenderModel model) {
        LOG.info("insertKeywordRenderRecord()");
        LOG.debug(model.toString());

        MapSqlParameterSource[] parameterSourceBatch = new MapSqlParameterSource[model.getAdTagAndKeywords().getDisplayedKeywords().size()];

        for (int i = 0; i < parameterSourceBatch.length; i++) {
            MapSqlParameterSource parameterSource = new MapSqlParameterSource();

            parameterSource.addValue("keywordText", model.getAdTagAndKeywords().getDisplayedKeywords().get(i));
            parameterSource.addValue("adTagHTMLId", model.getAdTagAndKeywords().getAdTagHTMLId());
            parameterSource.addValue("pageVisitID", model.getPageVisitID());

            parameterSourceBatch[i] = parameterSource;
        }

        LOG.info("inserting batch");
        return jdbcTemplate.batchUpdate(keywordRenderInsertSQL, parameterSourceBatch);
    }

    @TimeLogged
    public int insertKeywordClickRecord(KeywordClickModel model) {
        LOG.info("insertKeywordClickRecord()");
        LOG.debug(model.toString());

        MapSqlParameterSource parameterSource = new MapSqlParameterSource();

        parameterSource.addValue("keywordText", model.getClickedKeyword());
        parameterSource.addValue("adTagHTMLId", model.getAdTagHTMLId());
        parameterSource.addValue("browser", model.getBrowser());
        parameterSource.addValue("os", model.getOs());
        parameterSource.addValue("customerId", model.getCustomerId());
        parameterSource.addValue("country", model.getLocation().getCountry());
        parameterSource.addValue("state", model.getLocation().getState());
        parameterSource.addValue("url", model.getUrl());
        parameterSource.addValue("referer", model.getReferer());
        parameterSource.addValue("ip", model.getUserIP());

        return jdbcTemplate.update(keywordClickInsertSQL, parameterSource);
    }

    @TimeLogged
    public int insertAdClickRecord(AdClickModel model) {
        LOG.info("insertAdClickRecord()");
        LOG.debug(model.toString());

        MapSqlParameterSource parameterSource = new MapSqlParameterSource();

        parameterSource.addValue("adTitle", model.getAdTitle());
        parameterSource.addValue("browser", model.getBrowser());
        parameterSource.addValue("os", model.getOs());
        parameterSource.addValue("customerId", model.getCustomerId());
        parameterSource.addValue("country", model.getLocation().getCountry());
        parameterSource.addValue("state", model.getLocation().getState());
        parameterSource.addValue("url", model.getUrl());
        parameterSource.addValue("referer", model.getReferer());
        parameterSource.addValue("ip", model.getUserIP());

        return jdbcTemplate.update(adClickInsertSQL, parameterSource);
    }

    @TimeLogged
    public int insertPageVisitRecord(PageVisitModel model) {
        LOG.info("insertPageVisitRecord()");
        LOG.debug(model.toString());

        MapSqlParameterSource parameterSource = new MapSqlParameterSource();

        parameterSource.addValue("browser", model.getBrowser());
        parameterSource.addValue("os", model.getOs());
        parameterSource.addValue("customerId", model.getCustomerId());
        parameterSource.addValue("country", model.getLocation().getCountry());
        parameterSource.addValue("state", model.getLocation().getState());
        parameterSource.addValue("url", model.getPageURL());
        parameterSource.addValue("referer", model.getReferer());
        parameterSource.addValue("ip", model.getUserIP());

        Map<String, Object> resultMap = jdbcTemplate.queryForMap(pageVisitInsertSQL, parameterSource);
        return (int) resultMap.get("page_visit_id");
    }
}
