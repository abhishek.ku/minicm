package com.example.minicm.targeting.model;

public class SERStyleSetting {
    private SERStyles serStyles;
    private boolean isComplete;
    private int score;

    public SERStyles getSerStyles() {
        return serStyles;
    }

    public void setSerStyles(SERStyles serStyles) {
        this.serStyles = serStyles;
    }

    public boolean isComplete() {
        return isComplete;
    }

    public void setComplete(boolean complete) {
        isComplete = complete;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }
}
