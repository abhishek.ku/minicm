package com.example.minicm.targeting.model;

public class KeywordStyles {
    private String textColor;
    private String adDivBgColor;
    private String fontFamily;
    private String fontSize;
    private String textDecoration;

    public String getTextColor() {
        return textColor;
    }

    public void setTextColor(String textColor) {
        this.textColor = textColor;
    }

    public String getAdDivBgColor() {
        return adDivBgColor;
    }

    public void setAdDivBgColor(String adDivBgColor) {
        this.adDivBgColor = adDivBgColor;
    }

    public String getFontFamily() {
        return fontFamily;
    }

    public void setFontFamily(String fontFamily) {
        this.fontFamily = fontFamily;
    }

    public String getFontSize() {
        return fontSize;
    }

    public void setFontSize(String fontSize) {
        this.fontSize = fontSize;
    }

    public String getTextDecoration() {
        return textDecoration;
    }

    public void setTextDecoration(String textDecoration) {
        this.textDecoration = textDecoration;
    }

    public boolean isComplete() {
        return textColor != null && adDivBgColor != null && fontFamily != null && fontSize != null && textDecoration != null;
    }

}
