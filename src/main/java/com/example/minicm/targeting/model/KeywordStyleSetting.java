package com.example.minicm.targeting.model;

public class KeywordStyleSetting {
    private KeywordStyles keywordStyles;
    private boolean isComplete;
    private int score;

    public KeywordStyles getKeywordStyles() {
        return keywordStyles;
    }

    public void setKeywordStyles(KeywordStyles keywordStyles) {
        this.keywordStyles = keywordStyles;
    }

    public boolean isComplete() {
        return isComplete;
    }

    public void setComplete(boolean complete) {
        isComplete = complete;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }
}
