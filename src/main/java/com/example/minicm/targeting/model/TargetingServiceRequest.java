package com.example.minicm.targeting.model;

import com.example.minicm.utility.location.model.Location;

public class TargetingServiceRequest {
    private String customerId;
    private String browser;
    private String os;
    private Location location;
    private String adTagId;
    private String pattern;

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getBrowser() {
        return browser;
    }

    public void setBrowser(String browser) {
        this.browser = browser;
    }

    public String getOs() {
        return os;
    }

    public void setOs(String os) {
        this.os = os;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }


    public String getAdTagId() {
        return adTagId;
    }

    public void setAdTagId(String adTagId) {
        this.adTagId = adTagId;
    }

    public String getPattern() {
        return pattern;
    }

    public void setPattern(String pattern) {
        this.pattern = pattern;
    }

    @Override
    public String toString() {
        return "TargetingServiceRequest{" +
                "customerId='" + customerId + '\'' +
                ", browser='" + browser + '\'' +
                ", os='" + os + '\'' +
                ", location=" + location.toString() +
                ", adTagId='" + adTagId + '\'' +
                ", pattern='" + pattern + '\'' +
                '}';
    }
}
