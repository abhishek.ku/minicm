package com.example.minicm.targeting.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class SERStyles {
    @JsonProperty("ff")
    private String fontFamily;
    @JsonProperty("pc")
    private String primaryTextColor;
    @JsonProperty("sc")
    private String secondaryTextColor;
    @JsonProperty("bc")
    private String borderColor;
    @JsonProperty("br")
    private String borderRadius;
    @JsonProperty("lbgc")
    private String linkBGColor;
    @JsonProperty("lc")
    private String linkTextColor;
    @JsonProperty("lbr")
    private String linkBorderRadius;

    public String getFontFamily() {
        return fontFamily;
    }

    public void setFontFamily(String fontFamily) {
        this.fontFamily = fontFamily;
    }

    public String getPrimaryTextColor() {
        return primaryTextColor;
    }

    public void setPrimaryTextColor(String primaryTextColor) {
        this.primaryTextColor = primaryTextColor;
    }

    public String getSecondaryTextColor() {
        return secondaryTextColor;
    }

    public void setSecondaryTextColor(String secondaryTextColor) {
        this.secondaryTextColor = secondaryTextColor;
    }

    public String getBorderColor() {
        return borderColor;
    }

    public void setBorderColor(String borderColor) {
        this.borderColor = borderColor;
    }

    public String getBorderRadius() {
        return borderRadius;
    }

    public void setBorderRadius(String borderRadius) {
        this.borderRadius = borderRadius;
    }

    public String getLinkBGColor() {
        return linkBGColor;
    }

    public void setLinkBGColor(String linkBGColor) {
        this.linkBGColor = linkBGColor;
    }

    public String getLinkTextColor() {
        return linkTextColor;
    }

    public void setLinkTextColor(String linkTextColor) {
        this.linkTextColor = linkTextColor;
    }

    public String getLinkBorderRadius() {
        return linkBorderRadius;
    }

    public void setLinkBorderRadius(String linkBorderRadius) {
        this.linkBorderRadius = linkBorderRadius;
    }

    public boolean isComplete() {
        return fontFamily != null && primaryTextColor != null && secondaryTextColor != null &&
                borderColor != null && borderRadius != null && linkBGColor != null &&
                linkBorderRadius != null && linkTextColor != null;
    }
}
