package com.example.minicm.targeting;

import com.example.minicm.aspect.timeLogging.TimeLogged;
import com.example.minicm.targeting.model.*;
import com.example.minicm.utility.location.model.Location;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class TargetingRepo {
    private static final Logger LOG = LoggerFactory.getLogger(TargetingRepo.class);
    private NamedParameterJdbcTemplate jdbcTemplate;

    private String ksSelectSQL = "call sp_get_keyword_style_settings(:customerId, :browser, :os, :country, :state, :adTagId, :pattern)";
    private String ssSelectSQL = "call sp_get_ser_style_settings(:customerId, :browser, :os, :country, :state, :adTagId, :pattern)";

    @Autowired
    public void setDependencies(NamedParameterJdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @TimeLogged
    public List<KeywordStyleSetting> getKeywordStyleSettings(TargetingServiceRequest request) {
        LOG.info("getKeywordStyleSettings()");
        LOG.debug(request.toString());

        MapSqlParameterSource parameterSource = new MapSqlParameterSource();
        parameterSource.addValue("customerId", request.getCustomerId());
        parameterSource.addValue("browser", request.getBrowser());
        parameterSource.addValue("os", request.getOs());
        parameterSource.addValue("country", request.getLocation().getCountry());
        parameterSource.addValue("state", request.getLocation().getState());
        parameterSource.addValue("adTagId", request.getAdTagId());
        parameterSource.addValue("pattern", request.getPattern());

        return jdbcTemplate.query(ksSelectSQL, parameterSource, resultSet -> {
            List<KeywordStyleSetting> ksl = new ArrayList<>();

            while(resultSet.next()) {
                KeywordStyles ks = new KeywordStyles();
                ks.setAdDivBgColor(resultSet.getString("div_bg"));
                ks.setTextColor(resultSet.getString("text_color"));
                ks.setFontFamily(resultSet.getString("font_family"));
                ks.setFontSize(resultSet.getString("font_size"));
                ks.setTextDecoration(resultSet.getString("text_decoration"));

                KeywordStyleSetting kss = new KeywordStyleSetting();
                kss.setComplete(resultSet.getBoolean("is_complete"));
                kss.setScore(resultSet.getInt("score"));
                kss.setKeywordStyles(ks);

                ksl.add(kss);
            }

            return ksl;
        });
    }

    @TimeLogged
    public List<SERStyleSetting>  getSERStyleSettings(TargetingServiceRequest request) {
        LOG.info("getSERStyleSettings()");
        LOG.debug(request.toString());

        MapSqlParameterSource parameterSource = new MapSqlParameterSource();
        parameterSource.addValue("customerId", request.getCustomerId());
        parameterSource.addValue("browser", request.getBrowser());
        parameterSource.addValue("os", request.getOs());
        parameterSource.addValue("country", request.getLocation().getCountry());
        parameterSource.addValue("state", request.getLocation().getState());
        parameterSource.addValue("adTagId", request.getAdTagId());
        parameterSource.addValue("pattern", request.getPattern());

        return jdbcTemplate.query(ssSelectSQL, parameterSource, resultSet -> {
            List<SERStyleSetting> sssl = new ArrayList<>();

            while(resultSet.next()) {
                SERStyles styles = new SERStyles();
                styles.setPrimaryTextColor(resultSet.getString("primary_text_color"));
                styles.setSecondaryTextColor(resultSet.getString("secondary_text_color"));
                styles.setFontFamily(resultSet.getString("font_family"));
                styles.setBorderColor(resultSet.getString("border_color"));
                styles.setBorderRadius(resultSet.getString("border_radius"));
                styles.setLinkTextColor(resultSet.getString("link_text_color"));
                styles.setLinkBGColor(resultSet.getString("link_bg_color"));
                styles.setLinkBorderRadius(resultSet.getString("link_border_radius"));

                SERStyleSetting styleSetting = new SERStyleSetting();
                styleSetting.setComplete(resultSet.getBoolean("is_complete"));
                styleSetting.setScore(resultSet.getInt("score"));
                styleSetting.setSerStyles(styles);

                sssl.add(styleSetting);
            }

            return sssl;
        });
    }
}
