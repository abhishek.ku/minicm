package com.example.minicm.targeting;

import com.example.minicm.aspect.timeLogging.TimeLogged;
import com.example.minicm.targeting.model.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TargetingService {
    private static final Logger LOG = LoggerFactory.getLogger(TargetingService.class);
    private TargetingRepo targetingRepo;

    @Autowired
    public void setDependencies(TargetingRepo targetingRepo) {
        this.targetingRepo = targetingRepo;
    }

    @TimeLogged
    public KeywordStyles getKeywordStyles(TargetingServiceRequest request) {
        LOG.info("getKeywordStyles()");
        LOG.debug(request.toString());

        List<KeywordStyleSetting> kssl = targetingRepo.getKeywordStyleSettings(request);

        if (kssl.get(0).isComplete()) {
            return kssl.get(0).getKeywordStyles();
        } else {
            KeywordStyles resultantStyles = new KeywordStyles();
            int i = 0;

            do {
                KeywordStyles styles = kssl.get(i).getKeywordStyles();

                if (resultantStyles.getAdDivBgColor() == null) { resultantStyles.setAdDivBgColor(styles.getAdDivBgColor()); }
                if (resultantStyles.getFontFamily() == null) { resultantStyles.setFontFamily(styles.getFontFamily()); }
                if (resultantStyles.getFontSize() == null) { resultantStyles.setFontSize(styles.getFontSize()); }
                if (resultantStyles.getTextColor() == null) { resultantStyles.setTextColor(styles.getTextColor()); }
                if (resultantStyles.getTextDecoration() == null) { resultantStyles.setTextDecoration(styles.getTextDecoration()); }

                i += 1;
            } while (!resultantStyles.isComplete() && i < kssl.size());

            return resultantStyles;
        }
    }

    @TimeLogged
    public SERStyles getSERAdStyles(TargetingServiceRequest request) {
        LOG.info("getSERAdStyles()");
        LOG.debug(request.toString());

        List<SERStyleSetting> sssl = targetingRepo.getSERStyleSettings(request);

//        if (sssl.get(0).isComplete()) {
//            return sssl.get(0).getSerStyles();
//        } else {
            SERStyles resultantStyles = new SERStyles();
            int i = 0;
//            do {
//                SERStyles styles = sssl.get(i).getSerStyles();
//
//                if (resultantStyles.getPrimaryTextColor() == null) { resultantStyles.setPrimaryTextColor(styles.getPrimaryTextColor()); }
//                if (resultantStyles.getSecondaryTextColor() == null) { resultantStyles.setSecondaryTextColor(styles.getSecondaryTextColor()); }
//                if (resultantStyles.getFontFamily() == null) { resultantStyles.setFontFamily(styles.getFontFamily()); }
//                if (resultantStyles.getBorderColor() == null) { resultantStyles.setBorderColor(styles.getBorderColor()); }
//                if (resultantStyles.getBorderRadius() == null) { resultantStyles.setBorderRadius(styles.getBorderRadius()); }
//                if (resultantStyles.getLinkBGColor() == null) { resultantStyles.setLinkBGColor(styles.getLinkBGColor()); }
//                if (resultantStyles.getLinkBorderRadius() == null) { resultantStyles.setLinkBorderRadius(styles.getLinkBorderRadius()); }
//                if (resultantStyles.getLinkTextColor() == null) { resultantStyles.setLinkTextColor(styles.getLinkTextColor()); }
//
//                i += 1;
//            } while (!resultantStyles.isComplete() && i < sssl.size());

            return resultantStyles;
//        }
    }
}
