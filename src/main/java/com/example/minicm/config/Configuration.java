package com.example.minicm.config;

import com.example.minicm.keyword.helper.model.BGObject;
import com.example.minicm.keyword.helper.model.KObject;
import com.example.minicm.keyword.helper.model.KeywordLookupResponse;
import com.example.minicm.keyword.helper.model.RObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.core.env.Environment;
import org.springframework.web.client.RestTemplate;

import java.time.Duration;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@org.springframework.context.annotation.Configuration
public class Configuration {
    private Environment env;

    @Autowired
    public void setDependencies(Environment env) {
        this.env = env;
    }

    @Bean
    public ExecutorService getExecutorService() {
        return Executors.newCachedThreadPool();
    }

    @Bean("kbbRT")
    public RestTemplate getRestTemplateForKeywordApi(RestTemplateBuilder restTemplateBuilder) {
        return restTemplateBuilder.
                setConnectTimeout(Duration.ofSeconds(1)).
                setReadTimeout(Duration.ofSeconds(2)).build();
    }

    @Bean("serRT")
    public RestTemplate getRestTemplateForSER(RestTemplateBuilder restTemplateBuilder) {
        return restTemplateBuilder.
                setConnectTimeout(Duration.ofSeconds(3)).
                setReadTimeout(Duration.ofSeconds(2)).build();
    }

    @Bean
    public KeywordLookupResponse getDefaultKeywordLookupResponse() {
        List<KObject> k = new ArrayList<>();

        String[] kws = env.getProperty("defaultKws").split("\\|");
        for (String s: kws) {
            k.add(new KObject(s));
        }

        BGObject bgObject = new BGObject();
        bgObject.setC(15);
        bgObject.setK(k);

        List<BGObject> bg = new ArrayList<>();
        bg.add(bgObject);

        RObject rObject = new RObject();
        rObject.setP(100);
        rObject.setBg(bg);

        List<RObject> rObjects = new ArrayList<>();
        rObjects.add(rObject);

        List<List<RObject>> r = new ArrayList<>();
        r.add(rObjects);

        KeywordLookupResponse response = new KeywordLookupResponse();
        response.setR(r);

        return response;
    }
}
