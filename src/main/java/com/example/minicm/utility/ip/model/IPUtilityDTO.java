package com.example.minicm.utility.ip.model;

import java.util.Map;

public class IPUtilityDTO {
    private Map<String, String> headerMap;
    private String remoteAddress;

    public Map<String, String> getHeaderMap() {
        return headerMap;
    }

    public void setHeaderMap(Map<String, String> headerMap) {
        this.headerMap = headerMap;
    }

    public String getRemoteAddress() {
        return remoteAddress;
    }

    public void setRemoteAddress(String remoteAddress) {
        this.remoteAddress = remoteAddress;
    }

    @Override
    public String toString() {
        return "IPUtilityDTO{" +
                "headerMap=" + headerMap +
                ", remoteAddress='" + remoteAddress + '\'' +
                '}';
    }
}
