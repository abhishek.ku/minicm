package com.example.minicm.utility.ip;

import com.example.minicm.utility.ip.model.IPUtilityDTO;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

public interface IPUtility {
    String getClientIpAddress(IPUtilityDTO dto);
}
