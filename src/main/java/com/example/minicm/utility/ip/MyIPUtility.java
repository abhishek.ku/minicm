package com.example.minicm.utility.ip;

import com.example.minicm.aspect.timeLogging.TimeLogged;
import com.example.minicm.utility.ip.model.IPUtilityDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
public class MyIPUtility implements IPUtility {
    private static final Logger LOG = LoggerFactory.getLogger(MyIPUtility.class);

    private final String[] IP_HEADER_CANDIDATES = {
            "X-Forwarded-For",
            "Proxy-Client-IP",
            "WL-Proxy-Client-IP",
            "HTTP_X_FORWARDED_FOR",
            "HTTP_X_FORWARDED",
            "HTTP_X_CLUSTER_CLIENT_IP",
            "HTTP_CLIENT_IP",
            "HTTP_FORWARDED_FOR",
            "HTTP_FORWARDED",
            "HTTP_VIA",
            "REMOTE_ADDR" };

    @Override
    @TimeLogged
    public String getClientIpAddress(IPUtilityDTO dto) {
        LOG.info("invoked getClientIpAddress()");
        LOG.debug(dto.toString());

        Map<String, String> headerMap = dto.getHeaderMap();

        for (String header : IP_HEADER_CANDIDATES) {
            if (headerMap.containsKey(header)) {
                String ip = headerMap.get(header);
                if (ip != null && ip.length() != 0 && !"unknown".equalsIgnoreCase(ip)) {
                    return ip;
                }
            }
        }

        return dto.getRemoteAddress();
    }

}
