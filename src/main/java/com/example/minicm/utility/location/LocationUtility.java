package com.example.minicm.utility.location;

import com.example.minicm.utility.location.model.Location;

public interface LocationUtility {
    Location getLocationFromIP(String ip);
}
