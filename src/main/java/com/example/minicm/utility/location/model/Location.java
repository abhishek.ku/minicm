package com.example.minicm.utility.location.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Location {
    @JsonProperty("c")
    private String country;
    @JsonProperty("s")
    private String state;

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public Location(String country, String state) {
        this.country = country;
        this.state = state;
    }

    @Override
    public String toString() {
        return "Location{" +
                "country='" + country + '\'' +
                ", state='" + state + '\'' +
                '}';
    }
}
