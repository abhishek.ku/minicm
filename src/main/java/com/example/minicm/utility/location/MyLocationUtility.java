package com.example.minicm.utility.location;

import com.example.minicm.aspect.timeLogging.TimeLogged;
import com.example.minicm.utility.location.model.Location;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class MyLocationUtility implements LocationUtility {
    private static final Logger LOG = LoggerFactory.getLogger(MyLocationUtility.class);

    @Override
    @TimeLogged
    public Location getLocationFromIP(String ip) {
        LOG.info("invoked getLocationFromIP()");
        LOG.debug("ip: " + ip);

        Location location = new Location("US", "CA");
        return location;
    }
}
