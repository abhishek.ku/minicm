create database cm;
use cm;
create table ad_tag_details
(
    id int auto_increment
        primary key,
    html_tag_id varchar(50) null
);

create table browser_details
(
    id int auto_increment
        primary key,
    name varchar(100) null,
    constraint browser_details_name_uindex
        unique (name)
);

create table country_details
(
    id int auto_increment
        primary key,
    name varchar(100) null
);

create table country_states
(
    id int auto_increment
        primary key,
    country_id int null,
    state_name varchar(100) null,
    constraint country_states_country_id_state_name_uindex
        unique (country_id, state_name),
    constraint country_states_country_details_id_fk
        foreign key (country_id) references country_details (id)
);

create table keyword_details
(
    id int auto_increment
        primary key,
    text varchar(100) null
);

create table keyword_style
(
    id int auto_increment
        primary key,
    div_bg varchar(100) null,
    text_color varchar(20) null,
    font_family varchar(100) null,
    font_size varchar(20) null,
    text_decoration varchar(20) null
);

create table os_details
(
    id int auto_increment
        primary key,
    name varchar(100) null,
    constraint os_details_name_uindex
        unique (name)
);

create table page_details
(
    id int auto_increment
        primary key,
    url varchar(100) null
);

create table pattern_details
(
    id int auto_increment
        primary key,
    pattern varchar(100) null,
    constraint page_info_pattern_uindex
        unique (pattern)
);

create table priority_settings
(
    id int auto_increment
        primary key,
    domain int null,
    country_state int null,
    browser int null,
    os int null,
    ad_tag int null,
    page_pattern int null
);

create table domain_details
(
    id int auto_increment
        primary key,
    customer_id varchar(100) null,
    priority_settings_id int null,
    constraint domain_details_domain_uindex
        unique (customer_id),
    constraint domain_details_priority_settings_id_fk
        foreign key (priority_settings_id) references priority_settings (id)
);

create table keyword_click_record
(
    id int auto_increment
        primary key,
    keyword_id int null,
    domain_id int null,
    browser_id int null,
    os_id int null,
    page_id int null,
    ad_tag_id int null,
    country_state_id int null,
    referer_id int null,
    user_ip varchar(50) null,
    timestamp timestamp default CURRENT_TIMESTAMP null,
    constraint keyword_click_record_ad_tag_info_id_fk
        foreign key (ad_tag_id) references ad_tag_details (id),
    constraint keyword_click_record_browser_details_id_fk
        foreign key (browser_id) references browser_details (id),
    constraint keyword_click_record_country_states_id_fk
        foreign key (country_state_id) references country_states (id),
    constraint keyword_click_record_domain_details_id_fk
        foreign key (domain_id) references domain_details (id),
    constraint keyword_click_record_keyword_id_fk
        foreign key (keyword_id) references keyword_details (id),
    constraint keyword_click_record_os_details_id_fk
        foreign key (os_id) references os_details (id),
    constraint keyword_click_record_page_details_id_fk
        foreign key (page_id) references page_details (id),
    constraint keyword_click_record_page_details_id_fk_2
        foreign key (referer_id) references page_details (id)
);

create table page_visit_record
(
    id int auto_increment
        primary key,
    domain_id int null,
    browser_id int null,
    os_id int null,
    page_id int null,
    country_state_id int null,
    referer_id int null,
    user_ip varchar(50) null,
    timestamp timestamp default CURRENT_TIMESTAMP null,
    constraint page_visit_record_browser_details_id_fk
        foreign key (browser_id) references browser_details (id),
    constraint page_visit_record_country_states_id_fk
        foreign key (country_state_id) references country_states (id),
    constraint page_visit_record_domain_details_id_fk
        foreign key (domain_id) references domain_details (id),
    constraint page_visit_record_os_details_id_fk
        foreign key (os_id) references os_details (id),
    constraint page_visit_record_page_details_id_fk
        foreign key (page_id) references page_details (id),
    constraint page_visit_record_page_details_id_fk_2
        foreign key (referer_id) references page_details (id)
);

create table keyword_render_record
(
    id int auto_increment
        primary key,
    keyword_id int null,
    ad_tag_id int null,
    timestamp timestamp default CURRENT_TIMESTAMP null,
    page_visit_id int null,
    constraint keyword_render_record_ad_tag_info_id_fk
        foreign key (ad_tag_id) references ad_tag_details (id),
    constraint keyword_render_record_keyword_id_fk
        foreign key (keyword_id) references keyword_details (id),
    constraint keyword_render_record_page_visit_record_id_fk
        foreign key (page_visit_id) references page_visit_record (id)
);

create table ser_ad_details
(
    id int auto_increment
        primary key,
    ad_title varchar(200) null
);

create table ad_click_record
(
    id int auto_increment
        primary key,
    ad_id int null,
    domain_id int null,
    browser_id int null,
    os_id int null,
    page_id int null,
    country_state_id int null,
    referer_id int null,
    user_ip varchar(50) null,
    timestamp timestamp default CURRENT_TIMESTAMP null,
    constraint ad_click_record_ad_id_fk
        foreign key (ad_id) references ser_ad_details (id),
    constraint ad_click_record_browser_details_id_fk
        foreign key (browser_id) references browser_details (id),
    constraint ad_click_record_country_states_id_fk
        foreign key (country_state_id) references country_states (id),
    constraint ad_click_record_domain_details_id_fk
        foreign key (domain_id) references domain_details (id),
    constraint ad_click_record_os_details_id_fk
        foreign key (os_id) references os_details (id),
    constraint ad_click_record_page_details_id_fk
        foreign key (page_id) references page_details (id),
    constraint ad_click_record_page_details_id_fk_2
        foreign key (referer_id) references page_details (id)
);

create table serp_ad_style
(
    id int auto_increment
        primary key,
    font_family varchar(100) null,
    primary_text_color varchar(20) null,
    secondary_text_color varchar(20) null,
    link_bg_color varchar(20) null,
    link_text_color varchar(20) null,
    border_color varchar(20) null,
    border_radius varchar(20) null,
    link_border_radius varchar(20) null
);

create table serp_ad_style_mapping
(
    id int auto_increment
        primary key,
    pattern_id int null,
    browser_id int null,
    os_id int null,
    country_state_id int null,
    ad_tag_id int null,
    serp_ad_style_id int null,
    domain_id int null,
    constraint serp_ad_style_mapping_ad_tag_info_id_fk
        foreign key (ad_tag_id) references ad_tag_details (id),
    constraint serp_ad_style_mapping_browser_details_id_fk
        foreign key (browser_id) references browser_details (id),
    constraint serp_ad_style_mapping_country_states_id_fk
        foreign key (country_state_id) references country_states (id),
    constraint serp_ad_style_mapping_domain_details_id_fk
        foreign key (domain_id) references domain_details (id),
    constraint serp_ad_style_mapping_os_details_id_fk
        foreign key (os_id) references os_details (id),
    constraint serp_ad_style_mapping_page_info_id_fk
        foreign key (pattern_id) references pattern_details (id),
    constraint serp_ad_style_mapping_serp_ad_style_id_fk
        foreign key (serp_ad_style_id) references keyword_style (id)
);

create table style_mapping
(
    id int auto_increment
        primary key,
    page_id int null,
    browser_id int null,
    os_id int null,
    country_state_id int null,
    ad_tag_id int null,
    keyword_style_id int null,
    domain_id int null,
    constraint style_mapping_ad_tag_info_id_fk
        foreign key (ad_tag_id) references ad_tag_details (id),
    constraint style_mapping_browser_details_id_fk
        foreign key (browser_id) references browser_details (id),
    constraint style_mapping_country_states_id_fk
        foreign key (country_state_id) references country_states (id),
    constraint style_mapping_domain_details_id_fk
        foreign key (domain_id) references domain_details (id),
    constraint style_mapping_keyword_style_id_fk
        foreign key (keyword_style_id) references keyword_style (id),
    constraint style_mapping_os_details_id_fk
        foreign key (os_id) references os_details (id),
    constraint style_mapping_page_info_id_fk
        foreign key (page_id) references pattern_details (id)
);

create definer = root@localhost procedure sp_add_ad_click_record(IN ser_ad_title varchar(100), IN browser varchar(100), IN os varchar(100), IN cust_Id varchar(100), IN country varchar(100), IN state varchar(100), IN page_url varchar(100), IN referer varchar(100), IN ip varchar(50))
begin
    declare a_id int default -1;
    select id into a_id from ser_ad_details where ad_title = ser_ad_title;
    if (a_id = -1) then
        insert into ser_ad_details (ad_title) values (ser_ad_title);
        select last_insert_id() into a_id;
    end if;

    insert into ad_click_record
    (ad_id, domain_id, browser_id, os_id, page_id, country_state_id, referer_id, user_ip)
    VALUES (a_id,
            (select id from domain_details where customer_id = cust_Id),
            (select id from browser_details where name = browser),
            (select id from os_details where name = os),
            (select id from page_details where url = page_url),
            (select id
             from country_states
             where country_id = (select id from country_details where name = country) and state_name = state),
            (select id from page_details where url = referer),
            ip);
end;

create definer = root@localhost procedure sp_add_keyword_render_record(IN keyword_text varchar(100), IN ad_tag_html_id varchar(50), IN p_v_id int)
begin
    declare k_id int default -1;
    declare a_id int default (select id from ad_tag_details where html_tag_id = ad_tag_html_id);
    select id into k_id from keyword_details where text = keyword_text;
    if (k_id = -1) then
        insert into keyword_details (text) values (keyword_text);
        select last_insert_id() into k_id;
    end if;

    insert into keyword_render_record
    (keyword_id, ad_tag_id, page_visit_id)
    VALUES (k_id, a_id, p_v_id);
end;

create definer = root@localhost procedure sp_add_page_visit_record(IN browser_name varchar(100), IN os_name varchar(100), IN cust_id varchar(100), IN country_name varchar(100), IN st_name varchar(100), IN page_url varchar(100), IN referer_url varchar(100), IN ip varchar(50))
begin
    declare d_id int default (select id from domain_details where customer_id = cust_id);
    declare b_id int default (select id from browser_details where name = browser_name);
    declare o_id int default (select id from os_details where name = os_name);
    declare p_id int default -1;
    declare c_id int default (select id from country_details where name = country_name);
    declare l_id int default (select id from country_states where country_id = c_id and state_name = st_name);
    declare r_id int default -1;
    declare page_visit_id int default -1;

    select id into p_id from page_details where url = page_url;
    if (p_id = -1) then
        insert into page_details (url) values (page_url);
        select last_insert_id() into p_id;
    end if;

    select id into r_id from page_details where url = referer_url;
    if (r_id = -1) then
        insert into page_details (url) values (referer_url);
        select last_insert_id() into r_id;
    end if;

    insert into page_visit_record
    (domain_id, browser_id, os_id, page_id, country_state_id, referer_id, user_ip)
    values (d_id, b_id, o_id, p_id, l_id, r_id, ip);
    select last_insert_id() into page_visit_id;
    select page_visit_id;
end;

create definer = root@localhost procedure sp_get_keyword_style_settings(IN cust_id varchar(100), IN browser_name varchar(100), IN os_name varchar(100), IN country_name varchar(100), IN st_name varchar(100), IN ad_html_tag_id varchar(50), IN pg_pattern varchar(100))
begin
    declare d_id_any int default (select id from domain_details where customer_id = '*');
    declare d_id int default d_id_any;
    declare ps_id_any int default (select priority_settings_id from domain_details where customer_id = '*');
    declare ps_id int default ps_id_any;
    declare b_id_any int default (select id from browser_details where name = '*');
    declare b_id int default b_id_any;
    declare o_id_any int default (select id from os_details where name = '*');
    declare o_id int default o_id_any;
    declare c_id_any int default (select id from country_details where name = '*');
    declare c_id int default c_id_any;
    declare l_id_any int default (select id from country_states where country_id = c_id_any and state_name = '*');
    declare l_id int default l_id_any;
    declare a_id_any int default (select id from ad_tag_details where html_tag_id = '*');
    declare a_id int default a_id_any;
    declare p_id_any int default (select id from pattern_details where pattern = '*');
    declare p_id int default p_id_any;
    declare dp int default 1;
    declare bp int default 1;
    declare op int default 1;
    declare lp int default 1;
    declare ap int default 1;
    declare pp int default 1;

    select id, priority_settings_id into d_id, ps_id from domain_details where customer_id = cust_id;
    select domain, browser, os, country_state, ad_tag, page_pattern
    into dp, bp, op, lp, ap, pp
    from priority_settings
    where id = ps_id;
    select id into b_id from browser_details where name = browser_name;
    select id into o_id from os_details where name = os_name;
    select id into c_id from country_details where name = country_name;
    select id into l_id from country_states where country_id = c_id and state_name = '*';
    select id into l_id from country_states where country_id = c_id and state_name = st_name;
    select id into a_id from ad_tag_details where html_tag_id = ad_html_tag_id;
    select id into p_id from pattern_details where pattern like concat('%', pg_pattern, '%');

    select ks.*,
           (
                       if(sm.domain_id = d_id_any, 0, 1) * dp +
                       if(sm.browser_id = b_id_any, 0, 1) * bp +
                       if(sm.os_id = o_id_any, 0, 1) * op +
                       if(sm.country_state_id = l_id_any, 0, 1) * lp +
                       if(sm.ad_tag_id = a_id_any, 0, 1) * ap +
                       if(sm.page_id = p_id_any, 0, 1) * pp
               )                                    as score,
           if(isnull(div_bg) or isnull(font_family) or isnull(font_size) or isnull(text_color) or
              isnull(text_decoration), false, true) as is_complete
    from keyword_style ks,
         style_mapping sm
    where ks.id = sm.keyword_style_id
      and (sm.domain_id = d_id or sm.domain_id = d_id_any)
      and (sm.browser_id = b_id or sm.browser_id = b_id_any)
      and (sm.os_id = os_id or sm.os_id = o_id_any)
      and (sm.country_state_id = l_id or sm.country_state_id = l_id_any)
      and (sm.page_id = p_id or sm.page_id = p_id_any)
      and (sm.ad_tag_id = a_id or sm.ad_tag_id = a_id_any)
    order by score desc, is_complete desc;
end;

create definer = root@localhost procedure sp_get_ser_style_settings(IN cust_id varchar(100), IN browser_name varchar(100), IN os_name varchar(100), IN country_name varchar(100), IN st_name varchar(100), IN ad_html_tag_id varchar(50), IN pg_pattern varchar(100))
begin
    declare d_id_any int default (select id from domain_details where customer_id = '*');
    declare d_id int default d_id_any;
    declare ps_id_any int default (select priority_settings_id from domain_details where customer_id = '*');
    declare ps_id int default ps_id_any;
    declare b_id_any int default (select id from browser_details where name = '*');
    declare b_id int default b_id_any;
    declare o_id_any int default (select id from os_details where name = '*');
    declare o_id int default o_id_any;
    declare c_id_any int default (select id from country_details where name = '*');
    declare c_id int default c_id_any;
    declare l_id_any int default (select id from country_states where country_id = c_id_any and state_name = '*');
    declare l_id int default l_id_any;
    declare a_id_any int default (select id from ad_tag_details where html_tag_id = '*');
    declare a_id int default a_id_any;
    declare p_id_any int default (select id from pattern_details where pattern = '*');
    declare p_id int default p_id_any;
    declare dp int default 1;
    declare bp int default 1;
    declare op int default 1;
    declare lp int default 1;
    declare ap int default 1;
    declare pp int default 1;

    select id, priority_settings_id into d_id, ps_id from domain_details where customer_id = cust_id;
    select domain, browser, os, country_state, ad_tag, page_pattern
    into dp, bp, op, lp, ap, pp
    from priority_settings
    where id = ps_id;
    select id into b_id from browser_details where name = browser_name;
    select id into o_id from os_details where name = os_name;
    select id into c_id from country_details where name = country_name;
    select id into l_id from country_states where country_id = c_id and state_name = '*';
    select id into l_id from country_states where country_id = c_id and state_name = st_name;
    select id into a_id from ad_tag_details where html_tag_id = ad_html_tag_id;
    select id into p_id from pattern_details where pattern like concat('%', pg_pattern, '%');

    select ss.*,
           (
                       if(sasm.domain_id = d_id_any, 0, 1) * dp +
                       if(sasm.browser_id = b_id_any, 0, 1) * bp +
                       if(sasm.os_id = o_id_any, 0, 1) * op +
                       if(sasm.country_state_id = l_id_any, 0, 1) * lp +
                       if(sasm.ad_tag_id = a_id_any, 0, 1) * ap +
                       if(sasm.pattern_id = p_id_any, 0, 1) * pp
               ) as score,
           if(
                       isnull(primary_text_color) or isnull(font_family) or isnull(secondary_text_color) or
                       isnull(link_border_radius) or isnull(link_text_color) or isnull(link_bg_color) or
                       isnull(border_radius) or isnull(border_color), false, true
               ) as is_complete
    from serp_ad_style ss,
         serp_ad_style_mapping sasm
    where ss.id = sasm.serp_ad_style_id
      and (sasm.domain_id = d_id or sasm.domain_id = d_id_any)
      and (sasm.browser_id = b_id or sasm.browser_id = b_id_any)
      and (sasm.os_id = os_id or sasm.os_id = o_id_any)
      and (sasm.country_state_id = l_id or sasm.country_state_id = l_id_any)
      and (sasm.pattern_id = p_id or sasm.pattern_id = p_id_any)
      and (sasm.ad_tag_id = a_id or sasm.ad_tag_id = a_id_any)
    order by score desc, is_complete desc;
end;