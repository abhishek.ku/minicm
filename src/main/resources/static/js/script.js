const displaySEResults = (seResults) => {
    const listings = seResults.l;
    const styles = seResults.s;

    let innerHTML = `<ul style="list-style: none;font-family: ${styles.ff}">`;
    listings.forEach((e, i) => {
        innerHTML += `<li style="margin: 1rem 0;">
                            <div style="border: 1px solid ${styles.bc};border-radius: ${styles.br};padding: 1rem;">
                                <h4 style="margin: 0;color: ${styles.pc};">${e.title}</h4>
                                <p style="font-size: 0.75rem;color: ${styles.sc};">${e.description}</p>
                                <a onclick="adClick(${i})" style="background-color: ${styles.lbgc};color: ${styles.lc};padding: 0.5rem;text-decoration: none;border-radius: ${styles.lbr}" href="${e.clickUrl.value}">
                                    Click here
                                </a>
                            </div>
                          </li>`;
    });
    innerHTML += `</ul>`;

    const seResultsHolderDiv = document.getElementById("se-results-holder");
    seResultsHolderDiv.innerHTML = innerHTML;
};

let responseJSON;

const sendSerRequest = () => {
    const docURL = document.URL;
    const query = docURL.slice(docURL.indexOf('?'));

    const serRequest = new XMLHttpRequest();
    serRequest.open('GET', '/seResults' + query, true);

    serRequest.onreadystatechange = function () {
        if (this.readyState === 4 && this.status === 200) {
            responseJSON = JSON.parse(this.response);
            displaySEResults(responseJSON);
        }
    }

    serRequest.send();
};

const adClick = (adIndex) => {
    const urlSearchParams = new URLSearchParams(window.location.search);

    const reqBody = {
        adTitle: responseJSON.l[adIndex].title,
        customerId: urlSearchParams.get('cid'),
        browser: urlSearchParams.get('b'),
        os: urlSearchParams.get('o'),
        url: urlSearchParams.get('u'),
        referer: urlSearchParams.get('r')
    }

    const adClickRequest = new XMLHttpRequest();
    adClickRequest.open('POST', '/serpAdClick', true);
    adClickRequest.setRequestHeader("Content-Type", "application/json;charset=UTF-8");

    adClickRequest.send(JSON.stringify(reqBody));
};

sendSerRequest();