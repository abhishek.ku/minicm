(function () {
    const docUrl = document.URL;
    const referer = document.referrer;
    const serpURL = 'http://localhost:8080/serpage.html?';
    const kwUrl = 'http://localhost:8080/getKeywords';
    const renderRecordRequestURL = 'http://localhost:8080/addKeywordRenderRecord';
    const pageVisitRecordRequestURL = 'http://localhost:8080/addPageVisitRecord';

    const adTags = document.getElementsByTagName("my-adv");
    if (adTags && adTags.length > 0) {
        let pageVisitID = -1;
        let pageVisitRecordStatus = -1;

        const custId = adTags[0].getAttribute('cust-id');

        const getBrowserAndOsInfo = () => {
            const header = [navigator.platform, navigator.userAgent, navigator.appVersion, navigator.vendor, window.opera];
            const dataOS = [
                {name: 'Windows Phone', value: 'Windows Phone', version: 'OS'},
                {name: 'Windows', value: 'Win', version: 'NT'},
                {name: 'iPhone', value: 'iPhone', version: 'OS'},
                {name: 'iPad', value: 'iPad', version: 'OS'},
                {name: 'Kindle', value: 'Silk', version: 'Silk'},
                {name: 'Android', value: 'Android', version: 'Android'},
                {name: 'PlayBook', value: 'PlayBook', version: 'OS'},
                {name: 'BlackBerry', value: 'BlackBerry', version: '/'},
                {name: 'Macintosh', value: 'Mac', version: 'OS X'},
                {name: 'Linux', value: 'Linux', version: 'rv'},
                {name: 'Palm', value: 'Palm', version: 'PalmOS'}
            ];
            const dataBrowser = [
                {name: 'Edge', value: 'Edg', version: 'Edg'},
                {name: 'Chrome', value: 'Chrome', version: 'Chrome'},
                {name: 'Firefox', value: 'Firefox', version: 'Firefox'},
                {name: 'Safari', value: 'Safari', version: 'Version'},
                {name: 'Internet Explorer', value: 'MSIE', version: 'MSIE'},
                {name: 'Opera', value: 'Opera', version: 'Opera'},
                {name: 'BlackBerry', value: 'CLDC', version: 'CLDC'},
                {name: 'Mozilla', value: 'Mozilla', version: 'Mozilla'}
            ];
            const matchItem = (string, data) => {
                let i,
                    j = 0,
                    regex,
                    regexv,
                    match,
                    matches,
                    version;

                for (i = 0; i < data.length; i += 1) {
                    regex = new RegExp(data[i].value, 'i');
                    match = regex.test(string);
                    if (match) {
                        regexv = new RegExp(data[i].version + '[- /:;]([\\d._]+)', 'i');
                        matches = string.match(regexv);
                        version = '';
                        if (matches) {
                            if (matches[1]) {
                                matches = matches[1];
                            }
                        }
                        if (matches) {
                            matches = matches.split(/[._]+/);
                            for (j = 0; j < matches.length; j += 1) {
                                if (j === 0) {
                                    version += matches[j] + '.';
                                } else {
                                    version += matches[j];
                                }
                            }
                        } else {
                            version = '0';
                        }
                        return {
                            name: data[i].name,
                            version: parseFloat(version)
                        };
                    }
                }
                return {name: 'unknown', version: 0};
            };

            const agent = header.join(' '),
                os = matchItem(agent, dataOS),
                browser = matchItem(agent, dataBrowser);

            return {os: os, browser: browser};
        };
        const boInfo = getBrowserAndOsInfo();

        const sendPageVisitRecordRequest = () => {
            const request = new XMLHttpRequest();
            request.open('POST', pageVisitRecordRequestURL, true);
            request.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
            request.send(JSON.stringify({
                customerId: custId,
                pageURL: docUrl,
                referer: referer,
                browser: boInfo.browser.name,
                os: boInfo.os.name + ' ' + boInfo.os.version
            }));

            request.onreadystatechange = function () {
                if (this.readyState === 4) {
                    pageVisitRecordStatus = this.status;
                    if (this.status === 200) {
                        const response = JSON.parse(this.response);
                        pageVisitID = response.pageVisitID;
                    }
                }
            };
        };
        sendPageVisitRecordRequest();

        const sendKeywordRenderRecordRequest = (adRenderRecord) => {
            const request = new XMLHttpRequest();
            request.open('POST', renderRecordRequestURL, true);
            request.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
            request.send(JSON.stringify(adRenderRecord));
        };

        const renderAds = (kwResponse) => {
            const styleMap = kwResponse.ks;
            let keywordList = [];
            kwResponse.kl.forEach(e => {
                keywordList = keywordList.concat(e.k);
            });

            const shuffleKeywordList = () => {
                for (let i = 0; i < keywordList.length / 2; i++) {
                    let r = Math.round(Math.random() * (keywordList.length - 1));
                    let t = keywordList[i];
                    keywordList[i] = keywordList[r];
                    keywordList[r] = t;
                }
            };

            shuffleKeywordList();

            const adTagsAndDisplayedKeywords = [];

            for (let i = 0; i < adTags.length; i++) {
                const adTagHTMLId = adTags[i].getAttribute('id');
                const style = styleMap[adTagHTMLId];
                const displayedKeywords = [];
                let content = `<body style="overflow: hidden;"><ul style="padding-inline-start: 1rem;list-style-type: none;background-color: ${style.adDivBgColor}; font-size: ${style.fontSize}; font-family: ${style.fontFamily};">`;

                for (let j = 0; j < 5; j++) {
                    let kw = keywordList[((i * 5) + j) % keywordList.length];
                    displayedKeywords.push(kw.t);

                    const surl =
                        serpURL + 'ck=' + kw.t + '&cid=' + adTags[i].getAttribute('cust-id') +
                        '&u=' + docUrl + '&r=' + referer + '&atid=' + adTags[i].getAttribute('id') +
                        '&b=' + boInfo.browser.name + '&o=' + boInfo.os.name + ' ' + boInfo.os.version;
                    content += `<li style="margin: 1rem 0;"><a target="_blank" href="${surl}" style="color: ${style.textColor}; text-decoration: ${style.textDecoration};">${kw.t}</a></li>`;
                }

                content += '</ul></body>';

                const width = (adTagHTMLId === 'adr' || adTagHTMLId === 'adl') ? '300px' : '600px';
                const height = (adTagHTMLId === 'adr' || adTagHTMLId === 'adl') ? '300px' : '200px';

                adTags[i].innerHTML = `<iframe id="ad-frame-${i}" width="${width}" height="${height}" style="border: none; overflow: hidden;"></iframe>`;
                const iFrame = document.getElementById(`ad-frame-${i}`);
                iFrame.contentDocument.write(content);
                iFrame.contentDocument.close();

                adTagsAndDisplayedKeywords.push({
                    adTagHTMLId: adTagHTMLId,
                    displayedKeywords: displayedKeywords
                });
            }

            const interval = setInterval(() => {
                if (pageVisitID != -1) {
                    console.log('sending KeywordRenderRecordRequest');
                    const adRenderRecord = {
                        adTagsAndDisplayedKeywords: adTagsAndDisplayedKeywords,
                        pageVisitID: pageVisitID
                    }
                    sendKeywordRenderRecordRequest(adRenderRecord);
                    clearInterval(interval);
                } else if (pageVisitRecordStatus != -1 && pageVisitRecordStatus != 200) {
                    console.log('page visit record request failed');
                    clearInterval(interval);
                } else {
                    console.log('rechecking after 250ms');
                }
            }, 250);
        };

        const runRegex = (sentence) => {
            const regex = /\S{3,}/gm;
            const result = [];
            let m;

            while ((m = regex.exec(sentence)) !== null) {
                // This is necessary to avoid infinite loops with zero-width matches
                if (m.index === regex.lastIndex) {
                    regex.lastIndex++;
                }

                m.forEach((match) => {
                    result.push(match.toLowerCase());
                });
            }

            return result;
        }

        const getWords = () => {
            let words = [];

            let firstHeading = null;
            let pageDescription = null;
            let metaKeywords = null;

            const h1Elements = document.getElementsByTagName('h1');

            if (h1Elements && h1Elements.length > 0) {
                firstHeading = h1Elements[0].innerText;
                words = words.concat(runRegex(firstHeading));
            }

            const metaTags = document.getElementsByTagName('meta');

            if (metaTags && metaTags.length > 0) {
                if (metaTags.description) {
                    pageDescription = metaTags.description.content;
                    words = words.concat(runRegex(pageDescription));
                }
                if (metaTags.keywords) {
                    metaKeywords = metaTags.keywords.content;
                    words = words.concat(metaKeywords.split(','));
                }
            }

            words = words.concat(runRegex(document.title));

            return words;
        };

        const getAdKeywords = () => {
            const words = getWords();

            const adTagIdList = [];
            for (let i = 0; i < adTags.length; i++) {
                adTagIdList.push(adTags[i].getAttribute('id'));
            }

            const kwRequest = new XMLHttpRequest();
            kwRequest.open('POST', kwUrl, true);
            kwRequest.setRequestHeader("Content-Type", "application/json;charset=UTF-8");

            kwRequest.onreadystatechange = function () {
                if (this.readyState === 4 && this.status === 200) {
                    renderAds(JSON.parse(this.response));
                }
            };

            kwRequest.send(JSON.stringify({
                customerId: custId,
                adTagIdList: adTagIdList,
                url: docUrl,
                referer: referer,
                words: words,
                browser: boInfo.browser.name,
                os: boInfo.os.name + ' ' + boInfo.os.version
            }));
        };

        getAdKeywords();
    }
})();